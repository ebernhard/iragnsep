###################################
# Example of how to use iragnsep. #
# In this example we fit the      # 
# galaxy mrk1066, first including #
# the IRS data and then based on  #
# broadband photometry only.      #
###################################

# Import dependencies
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Import iragnsep
from iragnsep import run_all

import pdb
# redshift of mrk1066
z = 0.012

######
# Fit with IRS spectra
#####

# IRS spectra data
spec = pd.read_csv('./data_mrk1066_spec.csv')

wavIRS = spec['lambdamic'].values # wavelengths of the IRS [microns]
fluxIRS = spec['fluxJy'].values # fluxes of the IRS [Jy]
efluxIRS = spec['efluxJy'].values # uncertainties on the IRS fluxes [Jy]

# Herschel photometry
phot = pd.read_csv('./data_mrk1066.csv')

wav = np.array([70., 100., 160., 250., 350.]) # wavelengths of the available Herschel photometry [microns]
flux = np.array([phot['flux70'].values, phot['flux100'].values, \
				 phot['flux160'].values, phot['FLUX250'].values,\
				 phot['FLUX350'].values]).flatten()*1e-3 # Array of fluxes for the Herschel photometry [Jy]
eflux = np.array([phot['rms70'].values, phot['rms100'].values, \
				  phot['rms160'].values, phot['FLUX_ERR250'].values, \
         		  phot['FLUX_ERR350'].values]).flatten()*1e-3 # Array of uncertainties for the Herschel photometry [Jy]
filters = np.array(['PACS70', 'PACS100', 'PACS160', 'SPIRE250ps', 'SPIRE350ps']) # Array containing the names of the filters.

# The observed wavelengths, fluxes and uncertainties for the IRS spectra and the Herschel photometry are fed to iragnsep.
# The module run_all contains functions that fits, compare the models, analyse the results and plots.
# In particular, since we have the IRS spectrum of mrk1066, we use the function fitSpec of the module run_all.

# Keyword used here:
# z: redshift
# UL: indicates which of the photometric fluxes need to be set as an upper limit for the fits. UL needs to be of the same length as the data if passed to fitSpec.
# 	  Here we define the flux at 160 micron as an upper limit.
# Nmc: numbers of MCMC to perform. The more the better. From experience, 10000 is a very low limit. 50000 should be good enough. 100000 is good. 
# 		Here we use 10000.
# saveRes: If set to True, save the plots and the tables at pathFig+sourceName_fitRes_spec.pdf and pathFig+sourceName_fitRes_spec.csv, respectively.
res, resBM = run_all.fitSpec(wavIRS, fluxIRS, efluxIRS, \
							 wav, flux, eflux, \
							 filters, \
							 z = z, \
							 Nmc = 10000, \
							 ULPhot = np.array([0., 0., 1., 0., 0.]), \
							 sourceName = 'mrk1066', \
							 saveRes = True, \
							 pathTable = './', \
							 pathFig = './')

# OUTPUT
# res: dataframe containing the results of the fits.
# resBM: same as res but for the Best Model [BM] only.

# Print the best model SFR and the AIC weighted average SFR.
print('***************************')
print('The best model of mrk1066 returns SFR of ', np.round(resBM['SFR'].values,2), 'Msun/yr.')
print('The weighted average SFR of mrk1066 is ', np.round(np.sum(res['wSFR']),2), 'Msun/yr.')
print('***************************')

##################################################################

######
# Fit with photometry only
#####
wav_data = np.array([5.8, 8.0, 12.0, 22.0, 24., 70., 100., 160., 250., 350.]) # wavelengths of the photometry [micron]
flux_data = np.array([1.1e-1, 2.7e-1, 3.9e-1, 1.8, 1.9, 11.861, \
		              11.891, 7.848, 3.04, 1.26]) # fluxes from NED [Jy]
eflux_data = np.array([2.5e-4, 1.8e-4, 5.8e-3, 2.6e-2, 6e-2, 0.2,0.2, \
		               0.1, 0.04, 0.03]) # uncertainties on the fluxes from NED [Jy]
filters = np.array(['IRAC3', 'IRAC4', 'WISE_W3', 'WISE_W4', 'MIPS24', 'PACS70', 'PACS100', 'PACS160', 'SPIRE250ps', 'SPIRE350ps']) # Filters to be used in the fit
UL = np.zeros(len(filters)) # Define a vector of zeros for the upper limits.
UL[-3] = 1 # Set the 160micron flux as an upper limit.

# The observed wavelengths [wav_data], observed fluxes [flux_data], and their uncertainties [eflux_data] are fed to iragnsep (INPUT).
# The module run_all contains functions that fits, compare the models, analyse the results and plots.
# In particular, since we have photometry data alone of mrk1066, we use the function fitPhoto of the module run_all.

# Keyword used here:
# z: redshift
# UL: indicates which of the fluxes need to be set as an upper limit in the fits. UL has to be of the same length as the data.
# 	  Here we define the flux at 160 micron as upper limit.
# Nmc: numbers of MCMC to perform. The more the better. From experience, 10000 is good for photometric fits.
# saveRes:If set to True, save the plots and the tables at pathFig+sourceName_fitRes_photo.pdf and pathFig+sourceName_fitRes_photo.csv, respectively.
res, resBM = run_all.fitPhoto(wav_data, flux_data, eflux_data, \
							  filters, \
							  z = z, \
							  UL = UL, \
							  Nmc = 10000, \
						  	  sourceName = 'mrk1066', \
						  	  pathTable = './', \
						  	  pathFig = './', \
						  	  saveRes = True)

# OUTOUT
# res: dataframe containing the results of the fits.
# resBM: same as res but for the Best Model [BM] only.

# Print the best model SFR and the AIC weighted average SFR.
print('***************************')
print('The best model of mrk1066 returns SFR of ', np.round(resBM['SFR'].values,2), 'Msun/yr.')
print('The weighted average SFR of mrk1066 is ', np.round(np.sum(res['wSFR']),2), 'Msun/yr.')
print('***************************')

