import numpy as np
import pandas as pd
import os
import emcee
import pymc3 as pm
import theano.tensor as tt
import scipy
import math
import numba

from pymc3 import *
from emcee import EnsembleSampler
from .func import *
from .classes import *
from numba import njit, vectorize
from scipy.interpolate import interp1d

import time
import matplotlib.pyplot as plt
import corner

#########################################################
#														#
#		PHOTOMETRIC + SPEC VERSION OF THE FITTING		#
#														#
#########################################################
@vectorize(nopython=True)
def nberf(x):
    return math.erf(x)

@njit(fastmath=True)
def lnpostfn_spec_noAGN(theta, P, modelDust, modelPAH, y, ey, UL, wei):
	
	"""
    This function calculates the log-likelihood between spectral + photometric data and model without AGN contribution.
    ------------
    :param theta: vector containing the parameters.
    :param P: vector containing the priors for each of the parameters.
    :param modelBG: model dust continuum template.
    :param modelSG: model PAH template.
    :param fluxFit: observed fluxes.
    :param efluxFit: uncertainties on the observed fluxes.
    :param UL: vector contaning the upper-limits, if any.
    :param wei: weighting of the data points.
    :param z: redshift.
	:param wavFit: observed wavelengths.
    ------------
    :return logl: log-likelihood of the model knowing the parameters theta.
    """

    # Set a variable for the loglikelihood
	logl = 0

	# Prior constraint on the dust normalisation
	logl += -0.5*(theta[0]/P[0][1])**2.

	# Prior constraint on the PAH emission
	logl += -0.5*(theta[1]/P[1][1])**2.

	# Constraint on the 0.6 dex between the PAH and the dust continuum
	logl += -0.5*(theta[1] + P[1][0] - (0.97 * (theta[0] + P[0][0]) - 0.95))**2.

	# Define the model knowing the parameters theta
	ym = 10**(theta[0] + P[0][0]) * modelDust + 10**(theta[1] + P[1][0]) * modelPAH
	
	# Upper Limits
	x = 3.*(ym[UL == 1.] - y[UL == 1.])/y[UL == 1.]/np.sqrt(2)
	logl += np.sum(1. - (0.5 * (1. + nberf(x))) * wei[UL == 1.])

	# Detected fluxes
	logl += np.sum(-0.5 * ((y[UL == 0.] - ym[UL == 0.])/ey[UL == 0.])**2. * wei[UL == 0.])

	return logl


# @jit(nopython=True)
@njit(fastmath=True)
def lnpostfn_spec_wAGN(theta, P, modelDust, modelPAH, modelSi10, modelSi18, y, ey, UL, wei, z, wavFit, obsC, x_red):

	"""
    This function calculates the log-likelihood between spectral + photometric data and model that includes AGN contribution.
    ------------
    :param theta: vector containing the parameters.
    :param P: vector containing the priors for each of the parameters.
    :param modelBG: model dust continuum template.
    :param modelSG: model PAH template.
    :param fluxFit: observed fluxes.
    :param efluxFit: uncertainties on the observed fluxes.
    :param UL: vector contaning the upper-limits, if any.
    :param wei: weighting of the data points.
    :param z: redshift.
    :param wavFit: observed wavelengths.
    :param modelAGN: AGN model using the priors from the fit to calculate the final log-likelihood.
    ------------
    :return logl: log-likelihood of the model knowing the parameters theta.
    """

	# pdb.set_trace()

	# set a variable for the log-likelihood
	logl = 0

	# Normal prior on the norm of the dust
	logl += -0.5*(theta[0]/P[0][1])**2.

	# Normal prior on the norm of the PAH
	logl += -0.5*(theta[1]/P[1][1])**2.

	# Constraint on the 0.6 dex between the dust contrinuum and the PAH emission
	logl += -0.5*(theta[1] + P[1][0] - (0.97 * (theta[0] + P[0][0]) - 0.95))**2.

	# Normal prior on the AGN power law
	logl += -0.5*(theta[2]/P[2][1])**2.

	# Normal prior on alpha 1
	if -1. < theta[3] + P[3][0] < 10.:
		logl += -0.5*(theta[3]/P[3][1])**2.
	else:
		return -np.inf

	# Normal prior on alpha 2
	if -3.5 < theta[4] + P[4][0] < 3.:
		logl += -0.5*(theta[4]/P[4][1])**2.
	else:
		return -np.inf

	# Normal Prior on if alpha2 > alpha1
	if theta[4] > theta[3]:
		logl += -0.5*((theta[3]/theta[4] - 1.)/0.1)**2.
	# 	return -np.inf

	# Normal Prior on the normalisation of the Si emission at 10 microns
	logl += -0.5*(theta[5]/P[5][1])**2.

	# Normal Prior on the normalisation of the Si emission at 18 microns
	logl += -0.5*(theta[6]/P[6][1])**2.

	# Prior on the position of the breaks + boundaries
	if 20. <= theta[7]  + P[7][0] <= 500.:
		logl += -0.5*(theta[7]/P[7][1])**2.
	else:
		return -np.inf

	if -1. < theta[8] < 1.:
		pass
	else:
		return -np.inf

	lbreak_red = 10**np.arange(np.log10(max(30., theta[7] + P[7][0] - 10.)), np.log10(max(30., theta[7] + P[7][0] + 20.)), 0.05)
	lbreak_red[-1] = wavFit[-1]
	x_red_wlbreak = np.zeros(len(x_red)+len(lbreak_red))

	x_red_wlbreak[:len(x_red)] = x_red
	x_red_wlbreak[len(x_red):] = lbreak_red

	modelSi10 = 10**(theta[5] + P[5][0]) * Si10model_jit(x_red_wlbreak, theta[8])
	modelSi18 = 10**(theta[6] + P[6][0]) * Si18model_jit(x_red_wlbreak, theta[8])
	modelPL = 10**(theta[2] + P[2][0]) * contmodel_jit(np.array([theta[7] + P[7][0], theta[3] + P[3][0], theta[4] + P[4][0]]) , x_red_wlbreak)

	modelAGN = modelSi10 + modelSi18 + modelPL
	modelAGNFit = 10**np.interp(np.log10(wavFit/(1.+z)), np.log10(x_red_wlbreak), np.log10(modelAGN))

	# Calculate the model for the AGN given parameters theta.
	ym = modelAGNFit * obsC + 10**(theta[0] + P[0][0]) * modelDust + 10**(theta[1] + P[1][0]) * modelPAH

	# Upper limits
	x = 3.*(ym[UL == 1.] - y[UL == 1.])/y[UL == 1.]/np.sqrt(2)
	logl += np.sum(1. - (0.5 * (1. + nberf(x))) * wei[UL == 1.])

	# detected fluxes
	logl += np.sum(-0.5 * ((y[UL == 0.] - ym[UL == 0.])/ey[UL == 0.])**2. * wei[UL == 0.])

	return logl


import pdb
def runSEDspecFit(wavSpec, fluxSpec, efluxSpec,\
				  wavPhot, fluxPhot, efluxPhot, \
				  filters, \
				  z = -0.01,\
				  ULPhot = [], \
				  obsCorr = True,\
				  S9p7_fixed = -99., \
				  Nmc = 10000, pgrbar = 1, \
				  ExtCurve = 'iragnsep',\
				  Pdust = [11., 3.], PPAH = [9.7, 3.], \
				  PPL = [-1., 3.], Pbreak = [40., 10.], \
				  Palpha1 = [0., 1.], Palpha2 = [0., 1.],\
				  PSi10 = [-1., 3.], PSi18 = [-1., 3.],\
				  templ = ''):
	"""
    This function fits the observed SED when a spectrum is combined to photometric data. The observed wavelengths, 
    fluxes and uncertainties on the fluxes are passed separately for the spectrum and the photometric data.
    ------------
    :param wavSpec: observed wavelengths for the spectrum (in microns).
    :param fluxSpec: observed fluxes for the spectrum (in Jansky).
    :param efluxSpec: observed uncertainties on the fluxes for the spectrum (in Jansky).
    :param wavPhot: observed wavelengths for the photometry (in microns).
    :param fluxPhot: observed fluxes for the photometry (in Jansky).
    :param efluxPhot: observed uncertainties on the fluxes for the photometry (in Jansky).
    :param filters: name of the photometric filters to include in the fit.
    ------------
    :keyword z: redshift of the source. Default = 0.01.
    :keyword ULPhot: vector of length Nphot, where Nphot is the number of photometric data. If any of the value is set to 1, 
    the corresponding flux is set has an upper limit in the fit. Default = [].
    :keyword obsCorr: if set to True, iragnsep attempt to calculate the total silicate absorption at 9.7micron, 
    and to correct the observed fluxes for obscuration. Default = True
    :keyword S9p7_fixed: can be used to pass a fixed value for the total silicate absorption at 9.7 micron. Default = -99.
    :keyword Nmc: numer of MCMC run. Default = 10000.
    :keyword pgrbar: if set to 1, display a progress bar while fitting the SED. Default = 1.
    :keyword Pdust: normal prior on the log-normalisation of the galaxy dust continuum template. Default = [10., 3.] ([mean, std dev]).
    :keyword PPAH: normal prior on the log-normalisation of the PAH template. Default = [9., 3.] ([mean, std dev]).
	:keyword Ppl: normal prior on the log-normalisation AGN continuum (defined at 10 micron). Default = [-1., 3.] ([mean, std dev]).
	:keyword Pbreak: prior on lbreak, the position of the break. Default = [40., 1.] ([mean, std dev]).
	:keyword Pslope1: normal prior on alpha1, the slope of the first power-law defined between 1<lambda<15 microns. Default = [1., 2.] ([mean, std dev]).
	:keyword Pslope2: normal prior on alpha2, the slope of the second power-law defined between 15<lambda<lbreak. Default = [1., 2.] ([mean, std dev]).
	:keyword Plsg: normal prior on the log-normalisation of the silicate emission at 10micron. Default = [-1., 3.] ([mean, std dev]).
	:keyword Pllg: normal prior on the log-normalisation of the silicate emission at 18micron. Default = [-1., 3.] ([mean, std dev]).
	------------
    :return res_fit: dataframe containing the results of all the possible fits.
    :return res_fitBM: dataframe containing the results of the best fit only.
    """

	path_iragnsep = os.path.dirname(iragnsep.__file__)

	# Extract the names of the templates
	keys = templ.keys().values
	nameTempl_gal = []
	nameTempl_PAH = []
	for key in keys:
		if str(key).startswith('gal'):
			if str(key).endswith('PAH') == False:
				nameTempl_gal.append(key)
			else:
				nameTempl_PAH.append(key)

	# Test that we have template for everything (if no galaxy then it crashes)
	if len(nameTempl_gal) == 0:
		raise ValueError('The galaxy template does not exist. The name of the column defining nuLnu for the galaxy template needs to start with "gal".')

	# define the wavelengths
	try:
		wavTempl = templ['lambda_mic'].values
	except:
		raise ValueError('Rename the wavelengths column of the template "lambda_mic".')

	# Define the rest wavelengths for the photometric and the spectral data
	wavSpec_rest = wavSpec/(1. + z)
	wavPhot_rest = wavPhot/(1. + z)

	# Prepare the upper limits. Set a vector of zeros for the spectra. Set a vector fo zeros for the photometry if UL is underfined.
	if len(ULPhot) != len(wavPhot):
		ULPhot = np.zeros(len(wavPhot))
	ULSpec = np.zeros(len(wavSpec))
	UL = np.concatenate([ULSpec, ULPhot])

	# Concatenate the spectral and the photometric data.
	wavFit = np.concatenate([wavSpec, wavPhot])
	fluxFit = np.concatenate([fluxSpec, fluxPhot])
	efluxFit = np.concatenate([efluxSpec, efluxPhot])

	e_av_phot = np.mean(efluxPhot[ULPhot == 0.]/fluxPhot[ULPhot == 0.])
	e_av_spec = np.mean(efluxSpec[ULSpec == 0.]/fluxSpec[ULSpec == 0.])
	e_av_rat = e_av_spec/e_av_phot
	# print(e_av_spec, e_av_phot, e_av_rat)

	xrat = [1., 2., 4., 6.]
	yrat = [1., 7., 19., 30.]
	fit = np.polyfit(xrat, yrat, 1)

	# x = np.arange(0.5, 10., 0.1)

	# plt.plot(xrat, yrat, 'ro')
	# plt.plot(x, (fit[0] * x + fit[1]), 'b-')
	# plt.plot(e_av_rat, max((fit[0] * e_av_rat + fit[1]), 1.), 'go')
	# plt.show()
	# pdb.set_trace()

	multFact = max((fit[0] * e_av_rat + fit[1]), 1.)

	Specwei = np.zeros(len(wavSpec)) + (multFact)/len(wavSpec)
	Photwei = np.ones(len(wavPhot))
	wei = np.concatenate([Specwei, Photwei])

	xe = [0.011, 0.017, 0.034, 0.069]
	ye = [0.5, 1., 5., 15.]
	fit = np.polyfit(xe, ye, 2)
	
	# x = 10**np.arange(-5., 0., 0.01)

	# plt.plot(xe, ye, 'ro')
	# plt.plot(x, fit[0]*x**2. + fit[1]*x + fit[2], 'b-')
	# plt.plot(e_av_phot, fit[0]*e_av_phot**2. + fit[1]*e_av_phot + fit[2], 'go')
	# plt.show()
	# # plt.xscale('log')
	# plt.yscale('log')


	wei *= max(fit[0]*e_av_phot**2. + fit[1]*e_av_phot + fit[2], 1.)

	# pdb.set_trace()
	# wei *= 3.

	# efluxFit *= 0. + fluxFit * (np.mean(efluxSpec[ULSpec == 0.]/fluxSpec[ULSpec == 0.]) + np.mean(efluxPhot[ULPhot == 0.]/fluxPhot[ULPhot == 0.]))/2.


	# pdb.set_trace()

	# if 0.03 < np.mean(efluxFit[UL == 0.]/fluxFit[UL == 0.]) < 0.065:
	# 	wei *= 3.

	# if 0.065 < np.mean(efluxFit[UL == 0.]/fluxFit[UL == 0.]) < 0.07:
	# 	wei *= 5.

	# if 0.07 <= np.mean(efluxFit[UL == 0.]/fluxFit[UL == 0.]) < 0.11:
	# 	wei *= 10.

	# if np.mean(efluxFit[UL == 0.]/fluxFit[UL == 0.]) >= 0.11:
	# 	wei *= 100.



	# Correct for absorption, using the total 9.7micron absorption feature.
	if S9p7_fixed == -99.:
		# Test if there are actually data in the wavelength range, if not, continue the fit without correcting for obscuration
		o = np.where((wavSpec_rest > 9.) & (wavSpec_rest < 10.))[0]
		if (len(o) == 0.) & (obsCorr == True):
			print('*******************')
			print('It has failed to correct for obscuration. There is no data in the range required to correct for obscuration.' + \
				  ' The fit is continued without correcting for obscuration.')
			print('*******************')
			obsCorr = False

		# Correct for obscuration. If it somehow fails, continue the fit without correcting for obscuration.
		if obsCorr == True:
			try:
				S9p7 = obsC(wavSpec_rest, fluxSpec)
			except:
				S9p7 = -99.
				print('*******************')
				print('It has failed to correct the IRS spectrum for obscuration. The most likely explanation is '+ \
					  'redshift since it needs the restframe anchor ' + \
					  'wavelengths to measure the strength of the silicate absorption. The fit is continued without correcting for obscuration.')
				print('*******************')
				pass
		else:
			S9p7 = -99.
	else:
		S9p7 = S9p7_fixed

	# Open the extincrtion curve
	EC_wav, EC_tau = getExtCurve(ExtCurve)
	EC_wav_AGN, EC_tau_AGN = getExtCurve('PAHfit')
	if S9p7 > 0.:
		#Calculate tau9p7 for the galaxy
		tau9p7_gal = S9p7toTau9p7(S9p7, 'gal')
		tau9p7_AGN = S9p7toTau9p7(S9p7, 'AGN')

		# Galaxy
		tau = np.interp(wavFit/(1.+z), EC_wav, EC_tau) * tau9p7_gal
		obsPerWav_gal = ((1. - np.exp(-tau))/tau)

		#AGN
		tau = np.interp(wavFit/(1.+z), EC_wav_AGN, EC_tau_AGN) * tau9p7_AGN
		obsPerWav_AGN = np.exp(-tau)
	else:
		obsPerWav_gal = 1.
		obsPerWav_AGN = 1.

	# Define the free parameters
	logNorm_Dust_perTempl = [] #dust continuum
	elogNorm_Dust_perTempl = []
	logNorm_PAH_perTempl = [] #PAH emission
	elogNorm_PAH_perTempl = []
	
	logNorm_Si10_perTempl = [] #silicate at 10 micron
	elogNorm_Si10_perTempl = []
	logNorm_Si18_perTempl = [] #silicate at 18 micron
	elogNorm_Si18_perTempl = []
	logNormAGN_PL_perTempl = [] #norm AGN continuum
	elogNormAGN_PL_perTempl = []
	l_break_PL_perTempl = [] # position of the break for the PL
	el_break_PL_perTempl = []
	alpha1_perTempl = [] #slope of the first power law
	ealpha1_perTempl = []
	alpha2_perTempl = [] #slope of the second power law
	ealpha2_perTempl = []
	dSi_perTempl = []
	edSi_perTempl = []

	logl_perTempl = [] #log-likelihood of the mode
	tplName_perTempl = [] #Name of the template 
	S9p7_save = [] #total absorption at 9.7 micron
	AGNon = [] #flag for the use of the AGN in the fit
	nParms = [] # number of parameters

	x_red = 10**np.arange(np.log10(7), np.log10(30), 0.05)
	x_red[0] = wavFit.min()/(1.+z)

	# Fit looping over every of our galaxy templates
	for name_i in nameTempl_gal:
		assert isinstance(name_i, str), "The list nameTempl requests strings as it corresponds to the names" + \
										" given to the various templates of galaxies to use for the fit."

		if pgrbar == 1:
			print("****************************************")
			print("  Fit of "+name_i+" as galaxy template  ")
			print("****************************************")

		# Define synthetic fluxes for the dust continuum model at the observed wavelength to match the observed fluxes
		nuLnuBGTempl = templ[name_i].values

		Fnu = nuLnuToFnu(wavTempl, nuLnuBGTempl, z)
		fluxSpec_model = np.interp(wavSpec_rest, wavTempl, Fnu)

		SEDgen = modelToSED(wavTempl, nuLnuBGTempl, z)
		fluxPhot_model = []
		for filt in filters:
		 	fluxPhot_model.append(getattr(SEDgen, filt)())

		modelDust = np.concatenate([fluxSpec_model, fluxPhot_model]) * obsPerWav_gal

		# Define synthetic fluxes for the PAH model at the observed wavelength to match the observed fluxes.
		# When an empirical template is used, define a vector of zeros so that no PAH emission is accounted for.
		nuLnuSGTempl = templ[nameTempl_PAH[0]].values

		Fnu = nuLnuToFnu(wavTempl, nuLnuSGTempl, z)
		fluxSpec_model = np.interp(wavSpec_rest, wavTempl, Fnu)

		SEDgen = modelToSED(wavTempl, nuLnuSGTempl, z)
		fluxPhot_model = []
		for filt in filters:
			fluxPhot_model.append(getattr(SEDgen, filt)())

		modelPAH = np.concatenate([fluxSpec_model, fluxPhot_model])

		# fit without the AGN
		ndim = 2 #Number of paraneter
		nwalkers = int(2. * ndim) #number of walkers

		# Define the starting point of each of the parameters. Flat distrib. between -1 and 1. Parameters are normalised with a zero mean to ease convergence.
		parms = np.zeros(shape=(nwalkers, ndim))
		# parms[:,0] = np.random.uniform(low = -0.3, high = 0.3, size=nwalkers) # norm Dust
		# parms[:,1] = np.random.uniform(low = -0.3, high = 0.3, size=nwalkers) # norm PAH
		parms[:,0] = np.random.uniform(low = -0.3, high = 0.3, size=nwalkers) # norm Dust
		parms[:,1] = np.random.uniform(low = -0.3, high = 0.3, size=nwalkers) # norm PAH


		# Set the ensemble sampler of Goodman&Weare and run the MCMC for Nmc steps.
		sampler = EnsembleSampler(nwalkers, ndim, lnpostfn_spec_noAGN, \
								  moves=[emcee.moves.StretchMove(a = 2.)],\
								  args = (np.array([Pdust, PPAH]), modelDust, modelPAH, fluxFit, efluxFit, \
								  UL, wei))
		sampler.run_mcmc(parms, Nmc, progress=bool(pgrbar))
		
		# Build the flat chain after burning 20% of it and thinning to every 10 values.
		NburnIn = int(0.2 * Nmc)
		chain = sampler.get_chain(discard=NburnIn, thin=10, flat=True)
		dfChain = pd.DataFrame(chain)
		dfChain.columns = ['logNormDust', 'logNormPAH']
		
		# corner.corner(dfChain)
		# plt.show()

		# pdb.set_trace()

		# Maybe add a way to plot the prior distributions
		# plt.hist(chain[:,0]+Pdust[0], bins = 1000, alpha = 0.6, label = 'Dust cont. prior')
		# plt.hist(chain[:,1]+PPAH[0], bins = 1000, alpha = 0.6, label = 'PAH prior')
		# plt.legend()
		# plt.show()
		# pdb.set_trace()

		# Save the optimised parameters. Median of the posterior as best fitting value and std dev as 1sigma uncertainties.
		# Dust continuum
		logNorm_Dust_perTempl.append(dfChain['logNormDust'].median())
		elogNorm_Dust_perTempl.append(dfChain['logNormDust'].std())

		# PAH
		logNorm_PAH_perTempl.append(dfChain['logNormPAH'].median())
		elogNorm_PAH_perTempl.append(dfChain['logNormPAH'].std())

		# Norm AGN, here -99. since no AGN accounted for
		logNormAGN_PL_perTempl.append(-99.)
		elogNormAGN_PL_perTempl.append(-99.)

		# slope of the first power law, here -99. since no AGN accounted for
		alpha1_perTempl.append(-99.)
		ealpha1_perTempl.append(-99.)

		# slope of the second power law, here -99. since no AGN accounted for
		alpha2_perTempl.append(-99.)
		ealpha2_perTempl.append(-99.)

		# cut off or position of the break, here -99. since no AGN accounted for
		l_break_PL_perTempl.append(-99.)
		el_break_PL_perTempl.append(-99.)

		# silicate emisison at 10 microns, here -99. since no AGN accounted for
		logNorm_Si10_perTempl.append(-99.)
		elogNorm_Si10_perTempl.append(-99.)

		# silicate emisison at 18 microns, here -99. since no AGN accounted for
		logNorm_Si18_perTempl.append(-99.)
		elogNorm_Si18_perTempl.append(-99.)

		dSi_perTempl.append(-99.)
		edSi_perTempl.append(-99.)

		# Calculate the logl of the model
		logl = lnpostfn_spec_noAGN(np.array([logNorm_Dust_perTempl[-1], logNorm_PAH_perTempl[-1]]), \
								   np.array([Pdust, PPAH]), \
								   modelDust, modelPAH, \
								   fluxFit, efluxFit, \
								   UL, wei)

		logl_perTempl.append(logl)

		# Flag for the used of an AGN. Here 0., since no AGN is accounted for.
		AGNon.append(0.)

		# save the number of parameters
		nParms.append(2)

		# save the name of the template
		tplName_perTempl.append(name_i)
		
		# Save teh total absorption at 9.7 microns.
		S9p7_save.append(round(S9p7,3))

		# Fit including the full AGN model.
		ndim = 9 # Number of parms
		nwalkers = int(2. * ndim) # Number of walkers

		# Define the starting parms. Each of them has been normalised to a mean of zero to ease convergence.
		parms = np.zeros(shape=(nwalkers, ndim))
		parms[:,0] = np.random.uniform(low = -0.3, high = 0.3, size=nwalkers) # norm Dust
		parms[:,1] = np.random.uniform(low = -0.3, high = 0.3, size=nwalkers) # norm PAH
		parms[:,2] = np.random.uniform(low = -0.3, high = 0.3, size=nwalkers) # norm PL AGN
		parms[:,3] = np.random.uniform(low = -0.3, high = 0.3, size=nwalkers) # alpha1
		parms[:,4] = np.random.uniform(low = -0.3, high = 0.3, size=nwalkers) # alpha2
		parms[:,5] = np.random.uniform(low = -0.3, high = 0.3, size=nwalkers) # 10micron
		parms[:,6] = np.random.uniform(low = -0.3, high = 0.3, size=nwalkers) # 18micron
		parms[:,7] = np.random.uniform(low = -1., high = 1., size=nwalkers) # Break
		parms[:,8] = np.random.uniform(low = -0.3, high = 0.3, size=nwalkers) # Break

		# dshift = 5.
		# modelSi10 = Simodel(wavFit/(1.+z), 9.8, 18., -5., 0.6) * obsPerWav_AGN
		# modelSi10_shift = Simodel(wavFit/(1.+z), 9.8 + dshift, 18., -5., 0.6)
		
		# plt.plot(wavFit/(1.+z), modelSi10, 'r.')
		# plt.plot(wavFit/(1.+z), modelSi10_shift, 'b.')

		# plt.plot(wavFit/(1.+z) + dshift, modelSi10, 'c.')


		# plt.plot([9.7, 10.7, 11.7], [1., 1., 1.], 'g-')
		# plt.plot([10.7], [1.], 'go')

		# plt.yscale('log')
		# plt.xscale('log')
		# plt.show()

		# pdb.set_trace()
		# modelSi18 = Simodel(wavFit/(1.+z), 12., 70., -2.5, 0.1) * obsPerWav_AGN

		# Set the ensemble sample of Goodman&Weare and run the MCMC for Nmc steps
		sampler = EnsembleSampler(nwalkers, ndim, lnpostfn_spec_wAGN, \
								  moves=[emcee.moves.StretchMove(a = 2.)],\
								  args = (np.array([Pdust, PPAH, PPL, Palpha1, Palpha2, PSi10, PSi18, Pbreak]), \
								  modelDust, modelPAH, -1., -1., fluxFit, efluxFit, UL, wei, z, wavFit, obsPerWav_AGN, x_red))
		sampler.run_mcmc(parms, Nmc, progress=bool(pgrbar))

		# Build the flat chain after burning 20% of the chain and thinning to every 10 values.
		chain = sampler.get_chain(discard=NburnIn, thin=10, flat=True)

		dfChain = pd.DataFrame(chain)
		dfChain.columns = ['logNormDust', 'logNormPAH', 'logNormAGN', 'alpha1', 'alpha2', 'logNormSi10', 'logNormSi18', 'lBreak', 'dSi']
		# corner.corner(dfChain)
		# plt.show()

		# Save the optimised parameters. Median of the posterior as best fitting value and std dev as 1sigma uncertainties.
		# Dust continuum
		logNorm_Dust_perTempl.append(dfChain['logNormDust'].median())
		elogNorm_Dust_perTempl.append(dfChain['logNormDust'].std())

		# PAH
		logNorm_PAH_perTempl.append(dfChain['logNormPAH'].median())
		elogNorm_PAH_perTempl.append(dfChain['logNormPAH'].std())
		
		# Norm AGN, here -99. since no AGN accounted for
		logNormAGN_PL_perTempl.append(dfChain['logNormAGN'].median())
		elogNormAGN_PL_perTempl.append(dfChain['logNormAGN'].std())

		# slope of the first power law, here -99. since no AGN accounted for
		alpha1_perTempl.append(dfChain['alpha1'].median())
		ealpha1_perTempl.append(dfChain['alpha1'].std())

		# slope of the second power law, here -99. since no AGN accounted for
		alpha2_perTempl.append(dfChain['alpha2'].median())
		ealpha2_perTempl.append(dfChain['alpha2'].std())

		# silicate emisison at 10 microns, here -99. since no AGN accounted for
		logNorm_Si10_perTempl.append(dfChain['logNormSi10'].median())
		elogNorm_Si10_perTempl.append(dfChain['logNormSi10'].std())

		# silicate emisison at 18 microns, here -99. since no AGN accounted for
		logNorm_Si18_perTempl.append(dfChain['logNormSi18'].median())
		elogNorm_Si18_perTempl.append(dfChain['logNormSi18'].std())

		# cut off or position of the break, here -99. since no AGN accounted for
		l_break_PL_perTempl.append(dfChain['lBreak'].median())
		el_break_PL_perTempl.append(dfChain['lBreak'].std())

		# dSi
		dSi_perTempl.append(dfChain['dSi'].median())
		edSi_perTempl.append(dfChain['dSi'].std())

		# Clauclate the logl of the model
		theta = np.array([logNorm_Dust_perTempl[-1],\
						  logNorm_PAH_perTempl[-1],\
						  logNormAGN_PL_perTempl[-1],\
						  alpha1_perTempl[-1], \
						  alpha2_perTempl[-1], \
						  logNorm_Si10_perTempl[-1], \
						  logNorm_Si18_perTempl[-1], \
						  l_break_PL_perTempl[-1],\
						  dSi_perTempl[-1]
						  ])
		Pr = np.array([Pdust, PPAH, PPL, Palpha1, Palpha2, PSi10, PSi18, Pbreak])
		logl = lnpostfn_spec_wAGN(theta, Pr, modelDust, modelPAH, -1., -1., fluxFit, efluxFit, UL, wei, z, wavFit, obsPerWav_AGN, x_red)
		logl_perTempl.append(logl)

		# pdb.set_trace()

		# AGNon = 1 since AGN is accounted for
		AGNon.append(1.)

		# Save the number of parameters
		nParms.append(9)

		# Save the name of the galaxy template
		tplName_perTempl.append(name_i)
		# Save the total obscuration at 9.7 microns
		S9p7_save.append(round(S9p7,3))

		# pdb.set_trace()


		# # o = np.random.randint(0., high = len(wavSpec)+1, size = 10)
		# # modelDust_red = np.concatenate([modelDust[o], modelDust[-len(wavPhot):]])
		# # modelPAH_red = np.concatenate([modelPAH[o], modelPAH[-len(wavPhot):]])
		# # wavFit_red = np.concatenate([wavFit[o],wavFit[-len(wavPhot):]])


		# alpha = np.arange(0.1, 1.5, 0.1)
		# for alpha_i in alpha:
		# 	plt.plot(wavFit, 10**0. * np.array(contmodel(np.array([40., alpha_i, 0.0]),wavFit/(1.+z)), dtype = 'float64'))

		# plt.yscale('log')
		# plt.xscale('log')
		# plt.show()
		# pdb.set_trace()

		# plt.plot(wavFit, 10**0. * np.array(contmodel(np.array([40., 0.2, 0.0]),wavFit/(1.+z)), dtype = 'float64'))
		# plt.plot(wavFit, 10**0. * np.array(contmodel(np.array([40., 0.3, 0.0]),wavFit/(1.+z)), dtype = 'float64'))


		# modelSi10 = Simodel_jit(wavFit/(1.+z), 10.3, 12., -3.5, 3.) * obsPerWav_AGN
		# modelSi18 = Simodel_jit(wavFit/(1.+z), 16.5, 9., -2.5, 1.) * obsPerWav_AGN
		# modelPL = np.array(contmodel(np.array([30., 0.9, 0.0]),wavFit/(1.+z)), dtype = 'float64') * obsPerWav_AGN
		# # efluxFit = 0.1 * fluxFit
		# plt.errorbar(wavFit, fluxFit, yerr = efluxFit, fmt = '.', color = 'r', ecolor = 'k') 

		# fluxFit = (10.**11. * modelDust + 10**9.72 * modelPAH + 10.**-1.3 * modelPL + 10**-1.6 * modelSi10 + 10**-1.5 * modelSi18)
		# efluxFit = 0.1 * fluxFit
		# plt.plot(wavFit, 10**11. * modelDust + 10**9.72 * modelPAH, 'g.')
		# plt.plot(wavFit, 10**-1.3 * modelPL, 'c.')
		# plt.plot(wavFit, 10**-1.6 * modelSi10, 'b.')
		# plt.plot(wavFit, 10**-1.4 * modelSi18, 'b.')
		# plt.errorbar(wavFit, fluxFit, yerr = efluxFit, fmt = '.', color = 'k')
		# plt.yscale('log')
		# plt.xscale('log')
		# plt.show()

		# pdb.set_trace()

		# # Define model without AGNs
		# with pm.Model() as modelNoAGN:
			
		# 	# Randomly draw Ndata from the IRS spectra
		# 	# ind_red = np.concatenate([np.random.randint(0., high = len(wavSpec), size = 10), np.arange(len(wavFit)-len(wavPhot), len(wavFit))])

		# 	# Constraints on the galaxy parameters
		# 	lognormdust_offset = pm.TruncatedNormal('logNormDust_offset', mu = 0., sd = 1., upper = (20. - Pdust[0])/Pdust[1], lower = (1. - Pdust[0])/Pdust[1])
		# 	lognormdust = pm.Deterministic('logNormDust', Pdust[0] + lognormdust_offset * Pdust[1])

		# 	lognormpah_offset = pm.TruncatedNormal('logNormPAH_offset', mu = 0., sd = 1., upper = (20. - PPAH[0])/PPAH[1], lower = (1. - PPAH[0])/PPAH[1])
		# 	lognormpah = pm.Deterministic('logNormPAH', PPAH[0] + lognormpah_offset * PPAH[1])
			
		# 	# The model (No AGN)
		# 	ymodel = 10**lognormdust * modelDust + 10**lognormpah * modelPAH

		# 	pm.Normal('PAHtoDUST', mu = lognormpah, observed = 0.97 * lognormdust - 0.95, sd = 0.3)
		# 	pm.Potential('obs', logp(np.log10(fluxFit), efluxFit/fluxFit * 0.434, np.log10(ymodel), wei, UL))

		# 	step = pm.NUTS()
		# 	traceNoAGN = pm.sample(draws = 2000, tune = 5000, step = step, chains=2, cores=1)
		# 	# Tune = 5000 draws = 2000

		# # Define the silicate emission
		# modelSi10 = Simodel_jit(wavFit/(1.+z), 10.3, 12., -3.5, 3.) * obsPerWav_AGN
		# modelSi18 = Simodel_jit(wavFit/(1.+z), 16.5, 9., -2.5, 1.) * obsPerWav_AGN
		
		# myModel = my_model(contmodel, wavFit/(1.+z))
		# with pm.Model() as modelAGN:

		# 	# Randomly draw Ndata from the IRS spectra
		# 	# ind_red = np.concatenate([np.random.randint(0., high = len(wavSpec), size = 10), np.arange(len(wavFit)-len(wavPhot), len(wavFit))])

		# 	# Constraints on the galaxy parameters
		# 	lognormdust_offset = pm.TruncatedNormal('logNormDust_offset', mu = 0., sd = 1., upper = (20. - Pdust[0])/Pdust[1], lower = (1. - Pdust[0])/Pdust[1])
		# 	lognormdust = pm.Deterministic('logNormDust', Pdust[0] + lognormdust_offset * Pdust[1])

		# 	lognormpah_offset = pm.TruncatedNormal('logNormPAH_offset', mu = 0., sd = 1., upper = (20. - PPAH[0])/PPAH[1], lower = (1. - PPAH[0])/PPAH[1])
		# 	lognormpah = pm.Deterministic('logNormPAH', PPAH[0] + lognormpah_offset * PPAH[1])

		# 	# Constraints on the AGN PL
		# 	lognormagn_offset = pm.Normal('logNormAGN_offset', mu = 0., sd = 1.)
		# 	lognormagn = pm.Deterministic('logNormAGN', PPL[0] + lognormagn_offset * PPL[1])

		# 	lbreak_offset = pm.TruncatedNormal('lBreak_offset', mu = 0., sd = 1., lower = (30. - Pbreak[0])/Pbreak[1], upper = (500. - Pbreak[0])/Pbreak[1])
		# 	lbreak = pm.Deterministic('lBreak', Pbreak[0] + lbreak_offset * Pbreak[1])

		# 	alpha1_offset = pm.TruncatedNormal('alpha1_offset', mu = 0., sd = 1., upper = (5. - Palpha1[0])/Palpha1[1], lower = (-1. - Palpha1[0])/Palpha1[1])
		# 	alpha1 = pm.Deterministic('alpha1', Palpha1[0] + alpha1_offset * Palpha1[1])

		# 	alpha2_offset = pm.TruncatedNormal('alpha2_offset', mu = 0., sd = 1., upper = (3. - Palpha2[0])/Palpha2[1], lower = (-3.5 - Palpha2[0])/Palpha2[1])
		# 	alpha2 = pm.Deterministic('alpha2', Palpha2[0] + alpha2_offset * Palpha2[1])

		# 	lognormsi10_offset = pm.TruncatedNormal('logNormSi10_offset', mu = 0., sd = 1., upper = (5. - PSi10[0])/PSi10[1], lower = (-5. - PSi10[0])/PSi10[1])
		# 	lognormsi10 = pm.Deterministic('logNormSi10', PSi10[0] + lognormsi10_offset * PSi10[1])

		# 	lognormsi18_offset = pm.TruncatedNormal('logNormSi18_offset', mu = 0., sd = 1., upper = (5. - PSi18[0])/PSi18[1], lower = (-5. - PSi18[0])/PSi18[1])
		# 	lognormsi18 = pm.Deterministic('logNormSi18', PSi18[0] + lognormsi18_offset * PSi18[1])

		# 	#The model
		# 	#Galaxy model
		# 	modelGal = 10**lognormdust * modelDust + 10**lognormpah * modelPAH

		# 	# PL model
		# 	theta = tt.as_tensor_variable([lbreak, alpha1, alpha2])
		# 	modelpl = myModel(theta)
		# 	modelPL = (10**lognormagn * modelpl) * obsPerWav_AGN

		# 	# # # Silicate model
		# 	modelAGNSi10 = 10**lognormsi10 * modelSi10
		# 	modelAGNSi18 = 10**lognormsi18 * modelSi18

		# 	# Full model
		# 	ymodel =  modelAGNSi10  + modelAGNSi18 + modelGal + modelPL

		# 	# Constraints on the data
		# 	pm.Normal('PAHtoDUST', mu = lognormpah, observed = 0.97 * lognormdust - 0.95, sd = 0.3)
		# 	pm.Potential('obs', logp(np.log10(fluxFit), efluxFit/fluxFit * 0.434, np.log10(ymodel), wei, UL))

		# 	step1 = pm.NUTS([lognormdust, lognormpah, lognormagn, lognormsi10, lognormsi18])
		# 	step2 = pm.Metropolis([lbreak, alpha1, alpha2])
		# 	traceAGN = pm.sample(draws = 2000, tune = 10000, step = [step1, step2], chains=2, cores=1, discard_tuned_samples=True, compute_convergence_checks=True)			

		# 	#Tune = 10000 draws = 2000

		# samples = np.vstack([traceAGN[k] for k in ['logNormDust', 'logNormPAH', 'logNormAGN', 'lBreak', 'alpha1', 'alpha2', 'logNormSi10', 'logNormSi18']]).T
		# corner.corner(samples)
		# plt.show()

		# pdb.set_trace()
		# sample = pm.trace_to_dataframe(traceAGN, varnames = ['logNormDust', 'logNormPAH', 'logNormAGN', 'lBreak', \
		# 													 'alpha1', 'alpha2', 'logNormSi10', 'logNormSi18'])

		# logNormDust = np.mean(sample['logNormDust'].values.flatten())
		# logNormPAH = np.mean(sample['logNormPAH'].values.flatten())
		# logNormAGN = np.mean(sample['logNormAGN'].values.flatten())
		# lBreak_PL = np.mean(sample['lBreak'].values.flatten())
		# alpha1 = np.mean(sample['alpha1'].values.flatten())
		# alpha2 = np.mean(sample['alpha2'].values.flatten())
		# logNormSi10 = np.mean(sample['logNormSi10'].values.flatten())
		# logNormSi18 = np.mean(sample['logNormSi18'].values.flatten())

		# #  + 
		# fullModel = 10**(logNormDust) * modelDust + 10**(logNormPAH) * modelPAH + 10**(logNormSi10) * modelSi10 + 10**(logNormSi18) * modelSi18 + 10**logNormAGN * np.array(contmodel([lBreak_PL, alpha1, alpha2],wavFit/(1.+z)), dtype = 'float64')
		# plt.errorbar(wavFit[UL == 0.], fluxFit[UL == 0.], yerr = efluxFit[UL == 0.], fmt = '.', color = 'r', ecolor = 'k')
		# plt.errorbar(wavFit[UL == 1.], fluxFit[UL == 1.], yerr = efluxFit[UL == 1.], fmt = '.', color = 'r', ecolor = 'k', uplims = True)
		# plt.plot(wavFit, 10**(logNormDust) * modelDust + 10**(logNormPAH) * modelPAH, 'g.')
		# plt.plot(wavFit, 10**(logNormAGN) * np.array(contmodel([lBreak_PL, alpha1, alpha2], wavFit/(1.+z)), dtype = 'float64'), 'c.')
		# plt.plot(wavFit, 10**(logNormSi10) * modelSi10, 'g.')
		# plt.plot(wavFit, 10**(logNormSi18) * modelSi18, 'g.')
		# plt.plot(wavFit, fullModel, 'b.')
		# plt.yscale('log')
		# plt.xscale('log')
		# plt.show()

		# # # logNormDust_offset = np.mean(sample['logNormDust_offset_interval__'].values.flatten())
		# # # logNormPAH_offset = np.mean(sample['logNormPAH_offset_interval__'].values.flatten())
		# # # logNormAGN_offset = np.mean(sample['logNormAGN_offset'].values.flatten())
		# # # lBreak_offset = np.mean(sample['lBreak_offset_interval__'].values.flatten())
		# # # alpha1_offset = np.mean(sample['alpha1_offset_interval__'].values.flatten())
		# # # alpha2_offset = np.mean(sample['alpha2_offset_interval__'].values.flatten())
		# # # logNormSi10_offset = np.mean(sample['logNormSi10_offset_interval__'].values.flatten())
		# # # logNormSi18_offset = np.mean(sample['logNormSi18_offset_interval__'].values.flatten())


		# # # # Save the loglikelihood of the model without AGN
		# # # logl_perTempl.append(np.array([modelAGN.logp({'logNormDust_offset_interval__':logNormDust_offset, 'logNormPAH_offset_interval__': logNormPAH_offset,'logNormAGN_offset': logNormPAH_offset,'lBreak_offset_interval__': logNormPAH_offset,'alpha1_offset_interval__': alpha1_offset,'alpha2_offset_interval__': alpha2_offset,'logNormSi10_offset_interval__': -0.12790875892812772,'logNormSi18_offset_interval__': -0.22861929007023987})])[0])


		# pdb.set_trace()





		# # sample = pm.trace_to_dataframe(traceAGN, varnames = ['logNormDust', 'logNormPAH', 'logNormAGN', 'lBreak', 'alpha1', 'alpha2', 'logNormSi10', 'logNormSi18'])

		# # logNormDust = np.mean(sample['logNormDust'].values.flatten())
		# # logNormPAH = np.mean(sample['logNormPAH'].values.flatten())
		# # logNormAGN = np.mean(sample['logNormAGN'].values.flatten())
		# # lBreak_PL = np.mean(sample['lBreak'].values.flatten())
		# # alpha1 = np.mean(sample['alpha1'].values.flatten())
		# # alpha2 = np.mean(sample['alpha2'].values.flatten())
		# # logNormSi10 = np.mean(sample['logNormSi10'].values.flatten())
		# # logNormSi18 = np.mean(sample['logNormSi18'].values.flatten())


		# # # sample = pm.trace_to_dataframe(traceAGN, varnames = ['logNormDust', 'logNormPAH'])
		# # # logNormDust = np.mean(sample['logNormDust'].values.flatten())
		# # # logNormPAH = np.mean(sample['logNormPAH'].values.flatten())

		# # modelpl = np.array(contmodel(np.array([lBreak_PL, alpha1, alpha2]),wavFit/(1.+z)), dtype = 'float64') * obsPerWav_AGN


		# # fullModel = 10**(logNormDust) * modelDust + 10**(logNormPAH) * modelPAH + 10**(logNormAGN) * modelpl + 10**(logNormSi10) * modelSi10 + 10**(logNormSi18) * modelSi18
		# # plt.errorbar(wavFit, fluxFit, yerr = efluxFit, fmt = '.', color = 'r', ecolor = 'k')
		# # plt.plot(wavFit, 10**(logNormDust) * modelDust + 10**(logNormPAH) * modelPAH, 'b.')
		# # plt.plot(wavFit, 10**(logNormAGN) * modelpl, 'c.')
		# # plt.plot(wavFit, 10**(logNormSi10) * modelSi10, 'g.')
		# # plt.plot(wavFit, 10**(logNormSi18) * modelSi18, 'g.')
		# # plt.plot(wavFit, fullModel, 'ko')
		# # plt.yscale('log')
		# # plt.xscale('log')
		# # plt.show()

		# # pdb.set_trace()


		# # samples = np.vstack([traceAGN[k] for k in ['logNormDust', 'logNormPAH', 'logNormAGN', 'lBreak', 'alpha1', 'alpha2', 'logNormSi10', 'logNormSi18']]).T
		# # corner.corner(samples)
		# # plt.show()


		# # pdb.set_trace()


		# # pdb.set_trace()
		# # Summary statistics
		# sample = pm.trace_to_dataframe(traceNoAGN, varnames = ['logNormDust_offset', 'logNormDust', \
		# 													   'logNormPAH_offset', 'logNormPAH'])

		# logNormDust = sample['logNormDust'].values.flatten()
		# logNormPAH = sample['logNormPAH'].values.flatten()

		# logNorm_Dust_perTempl.append(np.mean(logNormDust))
		# elogNorm_Dust_perTempl.append(np.std(logNormDust))

		# logNorm_PAH_perTempl.append(np.mean(logNormPAH))
		# elogNorm_PAH_perTempl.append(np.std(logNormPAH))

		# # Norm AGN, here -99. since no AGN accounted for
		# logNormAGN_PL_perTempl.append(-99.)
		# elogNormAGN_PL_perTempl.append(-99.)

		# # slope of the first power law, here -99. since no AGN accounted for
		# alpha1_perTempl.append(-99.)
		# ealpha1_perTempl.append(-99.)

		# # slope of the second power law, here -99. since no AGN accounted for
		# alpha2_perTempl.append(-99.)
		# ealpha2_perTempl.append(-99.)

		# # cut off or position of the break, here -99. since no AGN accounted for
		# l_break_PL_perTempl.append(-99.)
		# el_break_PL_perTempl.append(-99.)

		# # silicate emisison at 10 microns, here -99. since no AGN accounted for
		# logNorm_Si10_perTempl.append(-99.)
		# elogNorm_Si10_perTempl.append(-99.)

		# # silicate emisison at 18 microns, here -99. since no AGN accounted for
		# logNorm_Si18_perTempl.append(-99.)
		# elogNorm_Si18_perTempl.append(-99.)


		# logNormDust_offset = np.mean(sample['logNormDust_offset'].values.flatten())
		# logNormPAH_offset = np.mean(sample['logNormPAH_offset'].values.flatten())

		# # Save the loglikelihood of the model without AGN
		# logl_perTempl.append(np.array([modelNoAGN.logp({'logNormDust_offset_interval__':logNormDust_offset, \
		# 												'logNormPAH_offset_interval__': logNormPAH_offset})])[0])
			
		# # Flag for the used of an AGN. Here 0., since no AGN is accounted for.
		# AGNon.append(0.)

		# # save the number of parameters
		# nParms.append(2)

		# # save the name of the template
		# tplName_perTempl.append(name_i)
		
		# # Save teh total absorption at 9.7 microns.
		# tau9p7_save.append(round(_tau9p7,3))

		# # Summary statistics
		# sample = pm.trace_to_dataframe(traceAGN, varnames = ['logNormDust_offset_interval__', 'logNormPAH_offset_interval__', 	'logNormAGN_offset', 'lBreak_offset_interval__', 'alpha1_offset_interval__', 'alpha2_offset_interval__', 'logNormSi10_offset_interval__', 'logNormSi18_offset_interval__', 'logNormDust', 'logNormPAH', 'logNormAGN', 'lBreak', 'alpha1', 'alpha2', 'logNormSi10', 'logNormSi18'])

		# logNormDust = sample['logNormDust'].values.flatten()
		# logNormPAH = sample['logNormPAH'].values.flatten()
		# logNormAGN = sample['logNormAGN'].values.flatten()
		# lBreak_PL = sample['lBreak'].values.flatten()
		# alpha1 = sample['alpha1'].values.flatten()
		# alpha2 = sample['alpha2'].values.flatten()
		# logNormSi10 = sample['logNormSi10'].values.flatten()
		# logNormSi18 = sample['logNormSi18'].values.flatten()
		# # sample = pm.trace_to_dataframe(traceAGN, varnames = ['logNormDust', 'logNormPAH'])
		# # logNormDust = np.mean(sample['logNormDust'].values.flatten())
		# # logNormPAH = np.mean(sample['logNormPAH'].values.flatten())

		# # fullModel = 10**(logNormDust) * modelDust + 10**(logNormPAH) * modelPAH + 10**(logNormAGN) * contmodel(wavFit/(1.+z), 20., lBreak_PL, alpha1, alpha2, -3.5) #+ 10**(logNormSi10) * modelSi10 + 10**(logNormSi18) * modelSi18
		# # plt.errorbar(wavFit, fluxFit, yerr = efluxFit, fmt = '.', color = 'r', ecolor = 'k')
		# # plt.plot(wavFit, 10**(logNormDust) * modelDust + 10**(logNormPAH) * modelPAH, 'b.')
		# # plt.plot(wavFit, 10**(logNormAGN) * contmodel(wavFit/(1.+z), 20., lBreak_PL, alpha1, alpha2, -3.5), 'c.')
		# # plt.plot(wavFit, 10**(logNormSi10) * modelSi10, 'g.')
		# # plt.plot(wavFit, 10**(logNormSi18) * modelSi18, 'g.')
		# # plt.plot(wavFit, fullModel, 'ko')
		# # plt.yscale('log')
		# # plt.xscale('log')
		# # plt.show()

		# # pdb.set_trace()




		# # samples = np.vstack([traceAGN[k] for k in ['logNormDust_offset', 'logNormPAH_offset', 'logNormAGN_offset', 'lBreak_offset', 'alpha1_offset', \
		# # 										   'alpha2_offset', 'logNormSi10_offset', 'logNormSi18_offset']]).T
		# # corner.corner(samples)
		# # plt.show()

		# # pdb.set_trace()


		# logNorm_Dust_perTempl.append(np.mean(logNormDust))
		# elogNorm_Dust_perTempl.append(np.std(logNormDust))

		# logNorm_PAH_perTempl.append(np.mean(logNormPAH))
		# elogNorm_PAH_perTempl.append(np.std(logNormPAH))

		# # Norm AGN, here -99. since no AGN accounted for
		# logNormAGN_PL_perTempl.append(np.mean(logNormAGN))
		# elogNormAGN_PL_perTempl.append(np.std(logNormAGN))

		# # slope of the first power law, here -99. since no AGN accounted for
		# alpha1_perTempl.append(np.mean(alpha1))
		# ealpha1_perTempl.append(np.std(alpha1))

		# # slope of the second power law, here -99. since no AGN accounted for
		# alpha2_perTempl.append(np.mean(alpha2))
		# ealpha2_perTempl.append(np.std(alpha2))

		# # cut off or position of the break, here -99. since no AGN accounted for
		# l_break_PL_perTempl.append(np.mean(lBreak_PL))
		# el_break_PL_perTempl.append(np.std(lBreak_PL))

		# # silicate emisison at 10 microns, here -99. since no AGN accounted for
		# logNorm_Si10_perTempl.append(np.mean(logNormSi10))
		# elogNorm_Si10_perTempl.append(np.std(logNormSi10))

		# # silicate emisison at 18 microns, here -99. since no AGN accounted for
		# logNorm_Si18_perTempl.append(np.mean(logNormSi18))
		# elogNorm_Si18_perTempl.append(np.std(logNormSi18))

		# logNormDust_offset = np.mean(sample['logNormDust_offset_interval__'].values.flatten())
		# logNormPAH_offset = np.mean(sample['logNormPAH_offset_interval__'].values.flatten())
		# logNormAGN_offset = np.mean(sample['logNormAGN_offset'].values.flatten())
		# lBreak_offset = np.mean(sample['lBreak_offset_interval__'].values.flatten())
		# alpha1_offset = np.mean(sample['alpha1_offset_interval__'].values.flatten())
		# alpha2_offset = np.mean(sample['alpha2_offset_interval__'].values.flatten())
		# logNormSi10_offset = np.mean(sample['logNormSi10_offset_interval__'].values.flatten())
		# logNormSi18_offset = np.mean(sample['logNormSi18_offset_interval__'].values.flatten())


		# # Save the loglikelihood of the model without AGN
		# logl_perTempl.append(np.array([modelAGN.logp({'logNormDust_offset_interval__':logNormDust_offset, \
		# 											  'logNormPAH_offset_interval__': logNormPAH_offset,\
		# 											  'logNormAGN_offset': logNormPAH_offset,\
		# 											  'lBreak_offset_interval__': logNormPAH_offset,\
		# 											  'alpha1_offset_interval__': alpha1_offset,\
		# 											  'alpha2_offset_interval__': alpha2_offset,\
		# 											  'logNormSi10_offset_interval__': logNormSi10_offset,\
		# 											  'logNormSi18_offset_interval__': logNormSi18_offset})])[0])

		# # Flag for the used of an AGN. Here 0., since no AGN is accounted for.
		# AGNon.append(1.)

		# # save the number of parameters
		# nParms.append(8)

		# # save the name of the template
		# tplName_perTempl.append(name_i)
		
		# # Save teh total absorption at 9.7 microns.
		# tau9p7_save.append(round(_tau9p7,3))

	# Find the best model and the Akaike weight
	bestModelInd, Awi = exctractBestModel(logl_perTempl, nParms, len(wavFit), corrected = False)
	bestModelFlag = np.zeros(len(AGNon))
	bestModelFlag[bestModelInd] = 1

	# Save the results in a table
	resDict = {'logNormGal_dust': np.array(logNorm_Dust_perTempl) + Pdust[0], 'elogNormGal_dust': np.abs(np.array(elogNorm_Dust_perTempl)), \
			   'logNormGal_PAH': np.array(logNorm_PAH_perTempl) + PPAH[0], 'elogNormGal_PAH': np.abs(np.array(elogNorm_PAH_perTempl)), \
			   'logNormAGN_PL': np.array(logNormAGN_PL_perTempl) + PPL[0], 'elogNormAGN_PL': np.abs(np.array(elogNormAGN_PL_perTempl)), \
			   'lBreak_PL': np.array(l_break_PL_perTempl) + Pbreak[0], 'elBreak_PL': np.abs(np.array(el_break_PL_perTempl )), \
			   'alpha1': np.array(alpha1_perTempl) + Palpha1[0], 'ealpha1': np.abs(np.array(ealpha1_perTempl)), \
			   'alpha2': np.array(alpha2_perTempl) + Palpha2[0], 'ealpha2': np.abs(np.array(ealpha2_perTempl)), \
			   'logNorm_Si10': np.array(logNorm_Si10_perTempl) + PSi10[0], 'elogNorm_Si10': np.abs(np.array(elogNorm_Si10_perTempl)), \
			   'logNorm_Si18': np.array(logNorm_Si18_perTempl) + PSi18[0], 'elogNorm_Si18': np.abs(np.array(elogNorm_Si18_perTempl)), \
			   'dSi': np.array(dSi_perTempl), 'edSi': np.abs(np.array(edSi_perTempl)), \
			   'logl': logl_perTempl, 'AGNon': AGNon, 'tplName': tplName_perTempl,\
			   'bestModelFlag': bestModelFlag, 'Aw': Awi, 'S9p7': S9p7_save}

	dfRes = pd.DataFrame(resDict)
	return dfRes


#################################################
#												#
#		PHOTOMETRIC VERSION OF THE FITTING		#
#												#
#################################################
@njit(fastmath=True)
def lnpostfn_photo_noAGN(theta, P, modelDust, modelPAH, UL, y, ey, wei):

	"""
    This function calculates the log-likelihood between photometric data and model without AGN contribution.
    ------------
    :param theta: vector containing the parameters.
    :param P: vector containing the priors for each of the parameters.
    :param modelBG: model dust continuum template.
    :param modelSG: model PAH template.
    :param UL: vector contaning the upper-limits, if any.
    :param fluxFit: observed fluxes.
    :param efluxFit: uncertainties on the observed fluxes.
    :param wei: weighting of the data points.
    ------------
    :return logl: log-likelihood of the model knowing the parameters theta.
    """

	# set the log-likelihood to zero
	logl = 0

	# prior constrain on the normalisation of the dust continuum for galaxy
	logl += -0.5*(theta[0]/P[0][1])**2.

	# prior constrain on the normalisation of the PAH
	logl += -0.5*(theta[1]/P[1][1])**2.

	# Constraint on the 1 dex between the PAH and the dust continuum
	logl += -0.5*((theta[1] + P[1][0] - (0.97 * (theta[0] + P[0][0]) - 0.95))/0.1)**2.

	# define the full model as PAH + dust continuum
	ym = 10**(theta[0] + P[0][0]) * modelDust + 10**(theta[1] + P[1][0]) * modelPAH

	# Upper Limits
	x = 3.*(ym[UL == 1.] - y[UL == 1.])/y[UL == 1.]/np.sqrt(2)
	logl += np.sum(1. - (0.5 * (1. + nberf(x))) * wei[UL == 1.])

	# Detected fluxes
	logl += np.sum(-0.5 * ((y[UL == 0.] - ym[UL == 0.])/ey[UL == 0.])**2. * wei[UL == 0.])

	return logl

@njit(fastmath=True)
def lnpostfn_photo_wAGN(theta, P, modelDust, modelPAH, modelAGN, modelSi, UL, y, ey, wei):
	
	"""
    This function calculates the log-likelihood between photometric data and model that includes AGN contribution.
    ------------
    :param theta: vector containing the parameters.
    :param P: vector containing the priors for each of the parameters.
    :param modelBG: model dust continuum template.
    :param modelSG: model PAH template.
    :param modelAGN: model AGN continuum template.
    :param modelSi: model silicate emission template.
    :param UL: vector contaning the upper-limits, if any.
    :param fluxFit: observed fluxes.
    :param efluxFit: uncertainties on the observed fluxes.
    :param wei: weighting of the data points.
    ------------
    :return logl: log-likelihood of the model knowing the parameters theta.
    """
	# set the log-likelihood to zero
	logl = 0

	# prior constrain on the normalisation of the dust continuum for galaxy
	logl += -0.5*(theta[0]/P[0][1])**2.
		
	# prior constrain on the normalisation of the PAH
	logl += -0.5*(theta[1])**2./P[1][1]**2.

	# constrain on 0.3 dex between the normalisation of the dust to that of PAHs
	logl += -0.5*((theta[1] + P[1][0] - (0.97 * (theta[0] + P[0][0]) - 0.95))/0.1)**2.

	# prior constrain on the normalisation of the AGN continuum
	logl += -0.5*(theta[2]/P[2][1])**2.

	# prior constrain on the normalisation of the silicate emission
	if theta[3] > 10.:
		return -np.inf
	logl += -0.5*(theta[3]/P[3][1])**2.

	# define the full model as PAH + dust continuum + AGN continuum + silicate emission
	ym = 10**(theta[0] + P[0][0]) * modelDust + 10**(theta[1] + P[1][0]) * modelPAH + 10**(theta[2] + P[2][0]) * modelAGN + 10**(theta[3] + P[3][0]) * modelSi

	# Upper Limits
	x = 3.*(ym[UL == 1.] - y[UL == 1.])/y[UL == 1.]/np.sqrt(2)
	logl += np.sum(1. - (0.5 * (1. + nberf(x))) * wei[UL == 1.])

	# Detected fluxes
	logl += np.sum(-0.5 * ((y[UL == 0.] - ym[UL == 0.])/ey[UL == 0.])**2. * wei[UL == 0.])

	return logl

def runSEDphotFit(lambdaObs, fluxObs, efluxObs, \
				  filters, \
				  z = 0.01, \
				  UL = [], \
				  S9p7 = -99.,\
				  ExtCurve = 'iragnsep', \
				  Nmc = 10000, pgrbar = 1, \
				  NoSiem = False, \
				  Pdust = [10., 3.], PPAH = [9., 3.], PnormAGN = [10., 3.], PSiEm = [10., 3.], \
				  templ = '', \
				  NOAGN = False):

	"""
    This function fits the observed photometric SED.
    ------------
    :param wav: observed wavelengths (in microns).
    :param fluxSpec: observed fluxes (in Jansky).
    :param efluxSpec: observed uncertainties on the fluxes (in Jansky).
    :param filters: name of the photometric filters to include in the fit.
    ------------
    :keyword z: redshift of the source. Default = 0.01.
    :keyword UL: vector of length Nphot, where Nphot is the number of photometric data. If any of the value is set to 1, 
    the corresponding flux is set has an upper limit in the fit. Default = [].
    :keyword Nmc: numer of MCMC run. Default = 10000.
    :keyword pgrbar: if set to 1, display a progress bar while fitting the SED. Default = 1.
	:keyword NoSiem: if set to True, no silicate emission template is included in the fit. Default = False.
    :keyword Pdust: normal prior on the log-normalisation of the galaxy dust continuum template. Default = [10., 3.] ([mean, std dev]).
    :keyword PPAH: normal prior on the log-normalisation of the PAH template. Default = [9., 3.] ([mean, std dev]).
    :keyword PnormAGN: normal prior on the log-normalisation of the AGN template. Default = [10., 3.] ([mean, std dev]).
    :keyword PSiem: normal prior on the log-normalisation of the silicate emission template. Default = [10., 3.] ([mean, std dev]).
    :keyword templ: normal prior on the log-normalisation of the silicate emission template. Default = [10., 3.] ([mean, std dev]).
    :keyword NOAGN: if set to True, fits are ran with SF templates only (i.e. no AGN emission is accounted for). Default = False.
	------------
    :return dfRes: dataframe containing the results of all the possible fits.
    """

	path_iragnsep = os.path.dirname(iragnsep.__file__)

	# If no templates are passed, open the Bernhard+20 templates.
	if len(templ) == 0:
		templ = pd.read_csv(path_iragnsep+'/iragnsep_templ.csv')

	# Extract the name of the templates
	keys = templ.keys().values
	nameTempl_gal = []
	nameTempl_PAH = []
	nameTempl_AGN = []
	nameTempl_Siem = []
	for key in keys:
		if str(key).startswith('gal'):
			if str(key).endswith('PAH') == False:
				nameTempl_gal.append(key)
			else:
				nameTempl_PAH.append(key)
		if str(key).startswith('AGN'):
			if str(key).endswith('Siem'):
				nameTempl_Siem.append(key)
			else:
				nameTempl_AGN.append(key)

	# Test that we have template for everything (if no galaxy then it crashes)
	if len(nameTempl_gal) == 0:
		raise ValueError('The galaxy template does not exist. The name of the column defining nuLnu for the galaxy template needs to start with "gal".')

	# define the wavelengths
	try:
		wavTempl = templ['lambda_mic'].values
	except:
		raise ValueError('Rename the wavelengths column of the template "lambda_mic".')

	# Correct for absorption, if fixed by the user.
	if S9p7 != -99.:
	
		# Get the extinction curve
		EC_wav, EC_tau = getExtCurve(ExtCurve)
		EC_wav_AGN, EC_tau_AGN = getExtCurve('PAHfit')

		_tau9p7 = S9p7toTau9p7(S9p7)

		tau = np.interp(lambdaObs/(1.+z), EC_wav, EC_tau) * _tau9p7
		obsPerWav = ((1. - np.exp(-tau))/tau)
		
		tau_AGN = np.interp(lambdaObs/(1.+z), EC_wav_AGN, EC_tau_AGN) * _tau9p7
		obsPerWav_AGN = ((1. - np.exp(-tau_AGN))/tau_AGN)

	else:
		_tau9p7 = -99.
		obsPerWav = 1.
		obsPerWav_AGN = 1.

	# calculate the uncertainties on the fluxes
	unc = efluxObs/fluxObs

	# define the weighting accordingly
	if len(lambdaObs) > 1.:
		wei = np.ones(len(lambdaObs))
		wei_i = np.ones(len(lambdaObs))
		wei_final = np.ones(len(lambdaObs))
		o = np.where(unc > 0.)[0]
		wei_i[o] = 1./unc[o]**2./np.sum(1./unc[o]**2.)
		wei[o] = (1./wei_i[o])/np.sum(1./wei_i[o]) * 10.
		wei_final = wei*np.gradient(lambdaObs) * 10.
	else:
		wei = np.ones(len(lambdaObs))
		wei_final = np.ones(len(lambdaObs))

	# Define a vectors of zeros if no upper limts are passed.
	if len(UL) != len(lambdaObs):
		UL = np.zeros(len(lambdaObs))

	o = np.where(UL == 0.)[0]
	if len(o) == 0.:
		UL = np.zeros(len(lambdaObs))
		efluxObs = fluxObs*0.1

	# Define the free parameters
	# Norm AGN continuum
	lnAGN_perTempl = []
	elnAGN_perTempl= []

	# Norm silicate emission
	lnSi_perTempl = []
	elnSi_perTempl = []

	# Norm dust continuum
	lnDust_perTempl = []
	elnDust_perTempl = []

	# Norm PAHs
	lnPAH_perTempl = []
	elnPAH_perTempl = []

	# final loglikelihood of the model
	logl_perTempl = []

	# name of the AGN and galaxy template
	tplNameGal_perTempl = []
	tplNameAGN_perTempl = []

	# if AGN is accounted for (1) or not (0)
	AGNon = []

	# Number of parameters in the model
	nParms = []

	# We loop over the 6 galaxy templates, first fitting only galaxy templates and then including the AGN templates.
	for name_i in nameTempl_gal:
		assert isinstance(name_i, str), "The list nameTempl requests strings as it corresponds to the names" + \
										" given to the various templates of galaxies to use for the fit."

		if pgrbar == 1:
			print("****************************************")
			print("  Fit of "+name_i+" as galaxy template  ")
			print("****************************************")


		# Define synthetic fluxes for the dust continuum model at the observed wavelength to match the observed fluxes
		nuLnuBGTempl = templ[name_i].values

		SEDgen = modelToSED(wavTempl, nuLnuBGTempl, z)
		fluxPhot_model = []
		for filt in filters:
		 	fluxPhot_model.append(getattr(SEDgen, filt)())

		modelDust = np.array(fluxPhot_model) * obsPerWav

		# Define synthetic fluxes for the PAH model at the observed wavelength to match the observed fluxes.
		# When an empirical template is used, define a vector of zeros so that no PAH emission is accounted for.
		nuLnuSGTempl = templ[nameTempl_PAH[0]].values

		SEDgen = modelToSED(wavTempl, nuLnuSGTempl, z)
		fluxPhot_model = []
		for filt in filters:
			fluxPhot_model.append(getattr(SEDgen, filt)())

		modelPAH = np.array(fluxPhot_model)

		# Perform the fit without the AGN contribution
		ndim = 2 # Number of free params
		nwalkers = int(2. * ndim) # Number of walkers

		# Define the parameters as flat distributions between -1 and 1. (We normalised to zero each parameters to ease convergence).
		parms = np.zeros(shape=(nwalkers, ndim))
		parms[:,0] = np.random.uniform(low = -0.3, high = 0.3, size=nwalkers) # norm Dust
		parms[:,1] = np.random.uniform(low = -0.3, high = 0.3, size=nwalkers) # norm PAH

		# Set the ensemble sampler of Goodman&Weare and run the MCMC for Nmc steps
		sampler = EnsembleSampler(nwalkers, ndim, lnpostfn_photo_noAGN, \
								  moves=[emcee.moves.StretchMove(a = 2.)],\
								  args = (np.array([Pdust, PPAH]), modelDust, modelPAH, UL, fluxObs, efluxObs, wei))
		sampler.run_mcmc(parms, Nmc, progress=bool(pgrbar))

		# Build the flat chain, after burning 20% of the chain and thinning to every 10 values.
		NburnIn = int(0.2 * Nmc)
		chain = sampler.get_chain(discard=NburnIn, thin=10, flat=True)

		# Save the best fit parameters. Median of the posterior is taken as the best fit parameter and the standard deviation as 1sigma uncertainties.
		# Norm dust continuum
		lnDust_perTempl.append(round(np.median(chain[:,0]),3))
		elnDust_perTempl.append(round(np.std(chain[:,0]),3))

		# Norm PAH emission (or -20. when the empirical template is used).
		lnPAH_perTempl.append(round(np.median(chain[:,1]),3))
		elnPAH_perTempl.append(round(np.std(chain[:,1]),3))

		# Calulate the final loglikelihood of the model, using the best fit parameters.
		logl_perTempl.append(round(lnpostfn_photo_noAGN(np.array([lnDust_perTempl[-1], lnPAH_perTempl[-1]]), np.array([Pdust, PPAH]), \
								   modelDust, modelPAH, UL, fluxObs, efluxObs, wei_final),3))
		
		# Norm on the silicate emission. -99. here since no AGN is accounted for.
		lnSi_perTempl.append(-99.)
		elnSi_perTempl.append(-99.)

		# Norm on the AGN template. -99. here since no AGN is accounted for.
		lnAGN_perTempl.append(-99.)
		elnAGN_perTempl.append(-99.)

		# No AGN in this fits, so AGNon set to zero
		AGNon.append(0.)
		
		# Save the numbers of parameters used in the fit, i.e. 2.
		nParms.append(2.)
		
		# Save the name of the template for the galaxy.
		tplNameGal_perTempl.append(name_i)
		
		# No AGN templates used in this fit, so name of the AGN template is set to 'N/A'.
		tplNameAGN_perTempl.append('N/A')

		if NOAGN != True:

			# Fit including the AGN. Loop over the two templates AGN A and AGN B.
			for AGN_i in nameTempl_AGN:

				# calculate the synthetic photometry of the AGN template at wavelengths lambdaObs.
				nuLnu_AGN = templ[AGN_i].values
				# generate the photometry
				SEDgen = modelToSED(wavTempl, nuLnu_AGN, z)
				modelAGN = []
				for filt in filters:
					modelAGN.append(getattr(SEDgen, filt)())

				modelAGN = np.array(modelAGN) * obsPerWav_AGN

				# calculate the synthetic photometry of the silicate template at wavelengths lambdaObs.
				# If no silicate emission is considered, set a vector of zeros so that no silicate emisison is account for.
				if NoSiem == False:
					modelSiem = []
					nuLnu_Siem = templ[nameTempl_Siem].values.flatten()
					SEDgen = modelToSED(wavTempl, nuLnu_Siem, z)
					for filt in filters:
						modelSiem.append(getattr(SEDgen, filt)())

					modelSiem = np.array(modelSiem) * obsPerWav_AGN
				else:
					modelSiem = modelAGN * 0.

				# Perform the fit with the AGN contribution
				ndim = 4 # Number of parmameters
				nwalkers = int(2. * ndim) # Number of walkers

				# Define the starting parameters as flat distributions between -1 and 1. We normalised each parameter to zero to each convergence.
				parms = np.zeros(shape=(nwalkers, ndim))
				parms[:,0] = np.random.uniform(low = -0.3, high = 0.3, size=nwalkers) # norm Dust
				parms[:,1] = np.random.uniform(low = -0.3, high = 0.3, size=nwalkers) # norm PAH
				parms[:,2] = np.random.uniform(low = -0.3, high = 0.3, size=nwalkers) # normAGN
				parms[:,3] = np.random.uniform(low = -0.3, high = 0.3, size=nwalkers) # normSi

				# Set the ensemble sampler of Goodman&Weare and run the MCMC for Nmc steps.
				sampler = EnsembleSampler(nwalkers, ndim, lnpostfn_photo_wAGN, \
										  moves=[emcee.moves.StretchMove(a = 2.)],\
										  args = (np.array([Pdust, PPAH, PnormAGN, PSiEm]), \
										  modelDust, modelPAH, modelAGN, modelSiem, UL, fluxObs, efluxObs, wei))
				sampler.run_mcmc(parms, Nmc, progress=bool(pgrbar))

				# Build the flat chain, after burning 20% of the chain and thinning to every 10 values in the chain.
				chain = sampler.get_chain(discard=NburnIn, thin=10, flat=True)


				# Save the best fit parameters. Median of the posterior is taken as the best fit parameter and the standard
				# deviation as 1sigma uncertainties.
				# Dust Normalisation
				lnDust_perTempl.append(round(np.median(chain[:,0]),3))
				elnDust_perTempl.append(round(np.std(chain[:,0]),3))

				# PAH normalisation
				lnPAH_perTempl.append(round(np.median(chain[:,1]),3))
				elnPAH_perTempl.append(round(np.std(chain[:,1]),3))
				
				# AGN continuum Norm
				lnAGN_perTempl.append(round(np.median(chain[:,2]),3))
				elnAGN_perTempl.append(round(np.std(chain[:,2]),3))

				# Si emission Norm
				lnSi_perTempl.append(round(np.median(chain[:,3]),3))
				elnSi_perTempl.append(round(np.std(chain[:,3]),3))
				
				# Numbers of params in the model
				nParms.append(4.)
	 
				# AGN accounted for in this case, so AGNon = 1
				AGNon.append(1.)

				# Name of the galaxy template
				tplNameGal_perTempl.append(name_i)

				# Name of the AGN template
				tplNameAGN_perTempl.append(AGN_i)

				# loglikelihood of the model
				logl_perTempl.append(round(lnpostfn_photo_wAGN(np.array([lnDust_perTempl[-1], lnPAH_perTempl[-1], lnAGN_perTempl[-1], \
										   lnSi_perTempl[-1]]), np.array([Pdust, PPAH, PnormAGN, PSiEm]), modelDust, modelPAH, modelAGN, \
										   modelSiem, UL, fluxObs, efluxObs, wei_final),3))

				if NoSiem == True:
					lnSi_perTempl[-1] = -20.
					elnSi_perTempl[-1] = 0.0


	# Find the best model and the Akaike weight amongst all the 18 possible fits by comparing their final loglikelihood
	bestModelInd, Awi = exctractBestModel(logl_perTempl, nParms, len(lambdaObs), corrected = True)
	bestModelFlag = np.zeros(len(AGNon))
	bestModelFlag[bestModelInd] = 1

	# Save the results in a table
	resDict = {'logNormGal_dust': np.array(lnDust_perTempl) + Pdust[0], 'elogNormGal_dust': np.abs(np.array(elnDust_perTempl)), \
			   'logNormGal_PAH': np.array(lnPAH_perTempl) + PPAH[0], 'elogNormGal_PAH': np.abs(np.array(elnPAH_perTempl)), \
			   'logNormAGN': np.array(lnAGN_perTempl) + PnormAGN[0], 'elogNormAGN': np.abs(np.array(elnAGN_perTempl)), \
			   'logNormSiem': np.array(lnSi_perTempl) + PSiEm[0], 'elogNormSiem': np.abs(np.array(elnSi_perTempl)), \
			   'logl': logl_perTempl, 'AGNon': AGNon, 'tplName_gal': tplNameGal_perTempl, 'tplName_AGN': tplNameAGN_perTempl,\
			   'bestModelFlag': bestModelFlag, 'Aw': Awi, 'tau9p7':_tau9p7}

	dfRes = pd.DataFrame(resDict)

	return dfRes