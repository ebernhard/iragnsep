import numpy as np
import os
import iragnsep
import glob
import pandas as pd
import theano.tensor as tt
import math
import numba

from autograd import *
from astropy.cosmology import WMAP9 as cosmo
from astropy import units as u
from astropy import constants as const
from scipy import integrate
from scipy.interpolate import UnivariateSpline, interp1d
from scipy.constants import h,k
from scipy.optimize import approx_fprime
from numba import njit, vectorize, float64, guvectorize, jit
from theano.compile.ops import as_op


c = const.c.value
Lsun = const.L_sun.value

path_iragnsep = os.path.dirname(iragnsep.__file__)

import pdb
def get_prop(df, z = 0.01, specOn = True, templ = ''):

	"""
    This function calculates the IR properties of the AGN and their hosts.
    ------------
    :param df: data-frame containing the results from the fits (i.e. optimised parameters) as returned by SEDanalysis.
    :param echar: characteristic uncertainties to calculate the uncertainties on the parameters.
    ------------
    :keyword z: redshift
	:keyword specOn: set to True if the data contain a spectrum in addition to the photometry.
    :keyword templ: set the templates that have been used in the fits.
    ------------
    :return loglum_hostIR: the host IR (8--1000microns) log-luminosity free of AGN contamination (Lsun).
    :return eloglum_hostIR: uncertainties on loglum_hostIR.
	:return loglum_hostMIR: the host IR (5--35microns) log-luminosity free of AGN contamination (Lsun).
    :return eloglum_hostIR: uncertainties on loglum_hostMIR.
	:return loglum_hostFIR: the host IR (40--1000microns) log-luminosity free of AGN contamination (Lsun).
    :return eloglum_hosFIR: uncertainties on loglum_hostFIR.
	:return loglum_AGNIR: the AGN IR log-luminosity free of host contamination (Lsun).
	:return loglum_AGNMIR: the AGN MIR log-luminosity free of host contamination (Lsun).
	:return loglum_AGNFIR: the AGN FIR log-luminosity free of host contamination (Lsun).
	:return AGNfrac_IR: the AGN fraction in the IR.
	:return AGNfrac_MIR: the AGN fraction in the MIR.
	:return AGNfrac_FIR: the AGN fraction in the FIR.
	:return SFR: the SFR of the galaxy free of AGN contamination.
	:return eSFR: the uncertainties on SFR.
	:return wSFR: the SFR of the galaxy free of AGN contamination weighted by its Akaike weight.
	:return ewSFR: the uncertainties on wSFR.
    """
	if len(templ) == 0:
		path = os.path.dirname(iragnsep.__file__)
		templ = pd.read_csv(path+'/iragnsep_templ.csv')

	# Extract the name of the templates
	keys = templ.keys().values
	nameTempl_gal = []
	nameTempl_AGN = []
	nameTempl_PAH = []
	nameTempl_Siem = []
	for key in keys:
		if str(key).startswith('gal'):
			if str(key).endswith('PAH') == False:
				nameTempl_gal.append(key)
			else:
				nameTempl_PAH.append(key)
		if str(key).startswith('AGN'):
			if str(key).endswith('Siem'):
				nameTempl_Siem.append(key)
			else:
				nameTempl_AGN.append(key)

	# Test that we have template for everything (if no galaxy then it crashes)
	if len(nameTempl_gal) == 0:
		raise ValueError('The galaxy template does not exist. The name of the column defining nuLnu for the galaxy template needs to start with "gal".')
	if len(nameTempl_AGN) == 0:
		print('Warning: The template for AGN is empty. The name of the column defining nuLnu for the AGN templates needs to start with "AGN".')

	# define the wavelengths
	try:
		wavTempl = templ['lambda_mic'].values
	except:
		raise ValueError('Rename the wavelengths column of the template "lambda_mic".')

	nu = c/wavTempl/1e-6 #Hz
	o_IR = np.where((nu > c/1000./1e-6) & (nu < c/8./1e-6))[0][::-1]
	o_MIR = np.where((nu > c/35./1e-6) & (nu < c/5./1e-6))[0][::-1]
	o_FIR = np.where((nu > c/1000./1e-6) & (nu < c/40./1e-6))[0][::-1]
	dMpc = cosmo.luminosity_distance(z).value
	dmeter = dMpc*u.Mpc.to(u.m)
	d2z = dmeter**2./(1.+z) # K correction=>https://ned.ipac.caltech.edu/level5/Sept02/Hogg/Hogg2.html
	JyToLsun = 1e-26 * 4. * np.pi * d2z/Lsun

	loglum_hostIR = []
	eloglum_hostIR = []
	loglum_hostMIR = []
	eloglum_hostMIR = []
	loglum_hostFIR = []
	eloglum_hostFIR = []
	loglum_AGNIR = []
	loglum_AGNMIR = []
	loglum_AGNFIR = []

	for i in range(0, len(df)):

		obj = df.iloc[i]

		normDust = 10**obj['logNormGal_dust']
		# enormDust = normDust * np.log(10) * obj['elogNormGal_dust']
		try:
			nuLnuDust = normDust * templ[obj['tplName']].values
			enuLnuDust = np.sqrt((nuLnuDust * np.log(10) * obj['elogNormGal_dust'])**2. + (normDust * templ['e'+obj['tplName']].values)**2.)
		except:
			nuLnuDust = normDust * templ[obj['tplName_gal']].values
			enuLnuDust = np.sqrt((nuLnuDust * np.log(10) * obj['elogNormGal_dust'])**2. + (normDust * templ['e'+obj['tplName_gal']].values)**2.)

		normPAH = obj['logNormGal_PAH']
		# enormPAH = normPAH * np.log(10) * obj['elogNormGal_PAH']
		nuLnuPAH = normPAH * templ['gal_PAH'].values
		enuLnuPAH = np.sqrt((nuLnuPAH * np.log(10) * obj['elogNormGal_PAH'])**2. + (normPAH * templ['egal_PAH'].values)**2.)

		LnuGal = (nuLnuDust + nuLnuPAH)/nu
		eLnuGal = np.sqrt(enuLnuDust**2. + enuLnuPAH**2.)/nu

		lum_hostIR_k = []
		lum_hostMIR_k = []
		lum_hostFIR_k = []
		for k in range(0, 1000):
			lum_hostIR_k.append(np.trapz(np.random.normal(LnuGal[o_IR], eLnuGal[o_IR]), nu[o_IR], dx = np.gradient(nu[o_IR])))
			lum_hostMIR_k.append(np.trapz(np.random.normal(LnuGal[o_MIR], eLnuGal[o_MIR]), nu[o_MIR], dx = np.gradient(nu[o_MIR])))
			lum_hostFIR_k.append(np.trapz(np.random.normal(LnuGal[o_FIR], eLnuGal[o_FIR]), nu[o_FIR], dx = np.gradient(nu[o_FIR])))

		loglum_hostIR.append(round(np.log10(np.median(lum_hostIR_k)),4))
		eloglum_hostIR.append(round(0.434 * np.std(lum_hostIR_k)/np.median(lum_hostIR_k) * 3.,4))
		
		loglum_hostMIR.append(round(np.log10(np.median(lum_hostMIR_k)),4))
		eloglum_hostMIR.append(round(0.434 * np.std(lum_hostMIR_k)/np.median(lum_hostMIR_k) * 3.,4))

		loglum_hostFIR.append(round(np.log10(np.median(lum_hostFIR_k)),4))
		eloglum_hostFIR.append(round(0.434 * np.std(lum_hostFIR_k)/np.median(lum_hostFIR_k) * 3.,4))

		if obj['AGNon'] == 1:
			if specOn == True:

				modelPL = 10**(obj['logNormAGN_PL']) * contmodel([obj['lBreak_PL'], obj['alpha1'], obj['alpha2']], wavTempl)
				modelSi10 = 10**(obj['logNorm_Si10']) * Si10model(wavTempl, obj['dSi'])
				modelSi18 = 10**(obj['logNorm_Si18']) * Si18model(wavTempl, obj['dSi'])

				LnuAGN = (modelPL + modelSi10 + modelSi18) * JyToLsun
				
				loglum_AGNIR.append(round(np.log10(np.trapz(LnuAGN[o_IR], nu[o_IR], dx = np.gradient(nu[o_IR]))),3)) #Lsun
				loglum_AGNMIR.append(round(np.log10(np.trapz(LnuAGN[o_MIR], nu[o_MIR], dx = np.gradient(nu[o_MIR]))),3)) #Lsun
				loglum_AGNFIR.append(round(np.log10(np.trapz(LnuAGN[o_FIR], nu[o_FIR], dx = np.gradient(nu[o_FIR]))),3)) #Lsun

			else:
				#AGN IR luminosity
				normAGN = 10**obj['logNormAGN']
				nuLnuAGN = normAGN * templ[obj['tplName_AGN']].values

				normSi = 10**obj['logNormSiem']
				nuLnuSi = normSi * templ[nameTempl_Siem].values.flatten()

				LnuAGN = (nuLnuAGN + nuLnuSi)/nu
				
				loglum_AGNIR.append(round(np.log10(np.trapz(LnuAGN[o_IR], nu[o_IR], dx = np.gradient(nu[o_IR]))),3)) #Lsun
				loglum_AGNMIR.append(round(np.log10(np.trapz(LnuAGN[o_MIR], nu[o_MIR], dx = np.gradient(nu[o_MIR]))),3)) #Lsun
				loglum_AGNFIR.append(round(np.log10(np.trapz(LnuAGN[o_FIR], nu[o_FIR], dx = np.gradient(nu[o_FIR]))),3)) #Lsun	

		else:

			loglum_AGNIR.append(0.0)
			loglum_AGNMIR.append(0.0)
			loglum_AGNFIR.append(0.0)

	#Ratio of luminosities
	loglum_hostIR = np.array(loglum_hostIR)
	eloglum_hostIR = np.array(eloglum_hostIR)
	loglum_hostMIR = np.array(loglum_hostMIR)
	eloglum_hostMIR = np.array(eloglum_hostMIR)
	loglum_hostFIR = np.array(loglum_hostFIR)
	eloglum_hostFIR = np.array(eloglum_hostFIR)
	
	loglum_AGNIR = np.array(loglum_AGNIR)
	loglum_AGNMIR = np.array(loglum_AGNMIR)
	loglum_AGNFIR = np.array(loglum_AGNFIR)

	AGNfrac_IR = np.round(10**loglum_AGNIR/(10**loglum_hostIR + 10**loglum_AGNIR),2)
	o = np.where(loglum_AGNIR == 0.)[0]
	AGNfrac_IR[o] = 0.

	AGNfrac_MIR = np.round(10**loglum_AGNMIR/(10**loglum_hostMIR + 10**loglum_AGNMIR),2)
	o = np.where(loglum_AGNMIR == 0.)[0]
	AGNfrac_MIR[o] = 0.

	AGNfrac_FIR = np.round(10**loglum_AGNFIR/(10**loglum_hostFIR + 10**loglum_AGNFIR),2)
	o = np.where(loglum_AGNFIR == 0.)[0]
	AGNfrac_FIR[o] = 0.

	SFR = np.round(1.09e-10 * 10**loglum_hostIR,3)
	eSFR = np.round(SFR * np.log(10) * eloglum_hostIR,3)
	wSFR = np.round(SFR * df['Aw'].values,3)
	ewSFR = np.round(eSFR * df['Aw'].values,3)
	
	return loglum_hostIR, eloglum_hostIR, \
		   loglum_hostMIR, eloglum_hostMIR, \
		   loglum_hostFIR, eloglum_hostFIR, \
		   loglum_AGNIR, loglum_AGNMIR, loglum_AGNFIR, \
		   AGNfrac_IR, AGNfrac_MIR, AGNfrac_FIR, SFR, eSFR, wSFR, ewSFR

def basictests(wavSpec, fluxSpec, efluxSpec, wavPhot, fluxPhot, efluxPhot, filters, z, specOn = True):

	"""
    This function runs some basic tests prior to run the main fitting code.
    ------------
    :param wavSpec: observed wavelengths for the spectrum (in microns).
    :param fluxSpec: observed fluxes for the spectrum (in Jansky).
    :param efluxSpec: observed uncertainties on the fluxes for the spectrum (in Jansky).
    :param wavPhot: observed wavelengths for the photometry (in microns).
    :param fluxPhot: observed fluxes for the photometry (in Jansky).
    :param efluxPhot: observed uncertainties on the fluxes for the photometry (in Jansky).
    :param filters: name of the photometric filters to include in the fit.
    :param z: redshift.
    ------------
	:keyword specOn: set to True if the data contain a spectrum in addition to the photometry.
    ------------
    :return 0
    """

	if (len(wavPhot) != len(fluxPhot)) or (len(wavPhot) != len(efluxPhot)):
		raise ValueError("PHOTOMETRY ISSUE: Crashed because wavelengths, fluxes and uncertainties on the fluxes have different lengths.")
	if len(filters) != len(wavPhot):
		raise ValueError("FILTERS ISSUE: Crashed because the number of filters provided does not correspond to the number of photometry points.")
	if (any(fluxPhot<0) == True) or (any(wavPhot<0) == True):
		raise ValueError("PHOTOMETRY ISSUE: Crash caused by some negative values in the wavelengths, fluxes or uncertainties on the fluxes.")
	if (any(fluxPhot != fluxPhot) == True) or (any(efluxPhot != efluxPhot) == True) or (any(wavPhot != wavPhot) == True):
		raise ValueError("PHOTOMETRY ISSUE: Crash caused by some non-numerical values in the wavelengths, fluxes or uncertainties on the fluxes.")
	if specOn == True:
		#test that the length of wavelength is the same as the data
		if (len(wavSpec) != len(fluxSpec)) or (len(wavSpec) != len(efluxSpec)):
			raise ValueError("SPECTRUM ISSUE: Crashed because wavelengths, fluxes and uncertainties on the fluxes have different lengths.")
		#test that there are no negative values
		if (any(fluxSpec<0) == True) or (any(efluxSpec<0) == True) or (any(wavSpec<0) == True):
			raise ValueError("SPECTRUM ISSUE: Crash caused by some negative values in the wavelengths, fluxes or uncertainties on the fluxes.")
		#test that there are NAN
		if (any(fluxSpec != fluxSpec) == True) or (any(efluxSpec != efluxSpec) == True) or (any(wavSpec != wavSpec) == True):
			raise ValueError("SPECTRUM ISSUE: Crash caused by some non-numerical values in the wavelengths, fluxes or uncertainties on the fluxes.")

	#test if the filter exists
	path = os.path.dirname(iragnsep.__file__) + '/Filters/'
	files = [f for f in glob.glob(path + "*.csv")]
	count = -1
	for f in [path+f+"Filter.csv" for f in filters]:
		count += 1
		if (f in files) == False:
			raise ValueError(" \n The filter "+ str(filters[count]) + " does not exist. This version does not allow you to add some filters." + \
							 " Please get in touch with us to add the required filters (e.p.bernhard@sheffield.ac.uk). Available filters are:" +\
							 " IRAC1 , IRAC2 , IRAC3 , IRAC4 , WISE_W1 , WISE_W2 , WISE_W3 , WISE_W4 , IRAS12, IRAS60, IRAS100 , MIPS24, MIPS70," +\
							 " MIPS160 , PACS70, PACS100 , PACS160, SPIRE250ps , SPIRE350ps , SPIRE500ps")

	#Test if the redshift has been given by the user
	if z<0:
		zdefault = input('Warning: The redshift is set to the default value of 0.01. The keyword "z" allows you to indicate the redshift of the source.\n '+\
						 ' Press enter to continue, or type "exit" to abort.\n')
		if zdefault == "exit":
			exit()
		else:
			pass

	# Test that the wavelengths are in ascening order
	if len(wavPhot) > 1:
		dlambda = np.gradient(wavPhot)
		o = np.where(dlambda < 0.)[0]
		if len(o) > 0.:
			raise ValueError('PHOTOMETRY ISSUE: Wavelenghts need to be in ascending order.')

	if (specOn == True):
		dlambda = np.gradient(wavSpec)
		o = np.where(dlambda < 0.)[0]
		if len(o) > 0.:
			raise ValueError('SPECTRUM ISSUE: Wavelenghts need to be in ascending order.')

	if specOn == True:
		# Test if it can concatenate the Spectra aand the photometry
		try:
			wav = np.concatenate([wavSpec, wavPhot])
		except:
			raise ValueError("WAVELENGTHS: Spectral data cannot be concatenated to photometric data. Please check data.")
		try:
			flux = np.concatenate([fluxSpec, fluxPhot])
		except:
			raise ValueError("FLUXES: Spectral data cannot be concatenated to photometric data. Please check data.")
		try:
			eflux = np.concatenate([efluxSpec, efluxPhot])
		except:
			raise ValueError("UNCERTAINTIES ON THE FLUXES: Spectral data cannot be concatenated to photometric data. Please check data.")
	else:
		# Break as not enough data points anyway,
		if len(wavPhot) < 4:
			if len(wavPhot) == 3:
				restWav = wavPhot/(1.+z)
				NFIR = len(np.where(restWav>50.)[0])
				if (NFIR > 0.) & (NFIR != len(wavPhot)):
					pass
				else:
					raise ValueError('There is not enough data points to fit the model when compared to the number of degrees of freedom. It needs a minimum of' + \
								 ' 3 photometric points, with at least 1 FIR (i.e. rest-wavelength>60micron) flux, including upper-limits.')
	
			else:
				raise ValueError('There is not enough data points to fit the model when compared to the number of degrees of freedom. It needs a minimum of' + \
								 ' 3 photometric points, with at least 1 FIR (i.e. rest-wavelength>60micron) flux, including upper-limits.')


def exctractBestModel(logl, k, n, corrected = True):

	"""
    This function extracts the best model and calculates the Akaike weights based on the log-likelihood returned by the fits.
    ------------
    :param logl: log-likelihood returned by the fits.
    :param k: number of free parameters.
    :param n: number of data points.
    ------------
	:keyword corrected: if set to True, calculates the corrected AIC for small number of data points.
    ------------
    :return bestModelInd: the index of the best model fit.
    :return Awi: Akaike weights of each of the models, with respect to the best model.
    """

	nkdif = np.array(n)-np.array(k)
	o = np.where(nkdif == 1)[0]
	if len(o) > 0:
		corrected = False

	if corrected == True:
		AIC = 2*np.array(k) - 2.*np.array(logl) + (2.*np.array(k)**2. + 2.*np.array(k))/(np.array(n)-np.array(k)-1.)
	else:
		AIC = 2*np.array(k) - 2.*np.array(logl)

	bestModelInd = np.where(AIC == np.min(AIC))[0]
	AICmin = AIC[bestModelInd][0]
	AwiNorm = np.sum(np.exp(-0.5 * (AIC-AICmin)))

	Awi = np.exp(-0.5 * (AIC-AICmin))/AwiNorm

	return bestModelInd, Awi

def nuLnuToFnu(spec_wav, nuLnu, z):

	"""
    This function calculates the observed flux from nuLnu.
    ------------
    :param spec_wav: rest-wavelengths (in microns).
    :param nuLnu: nuLnu.
    :param z: redshift.
    ------------
    :return Fnu: observed flux on Earth of the source located at redshift z (in Jansky).
    """

	dMpc = cosmo.luminosity_distance(z).value #Mpc
	dmeter = dMpc*u.Mpc.to(u.m)
	d2z = dmeter**2./(1.+z) # K correction=>https://ned.ipac.caltech.edu/level5/Sept02/Hogg/Hogg2.html

	# Derive the observed flux
	nu = c/spec_wav/1e-6 #Hz
	Lnu = nuLnu/nu*Lsun #W/Hz
	Fnu = Lnu/4./np.pi/d2z #W/Hz/m2

	return Fnu * 1e26 # Jy

def getFluxInFilt(filt_wav, filt_QE, spec_wav, nuLnu, z, Fnu = False):

	"""
    This function calculates the synthetic flux in a given filter and at a given redshift from a source with luminosity nuLnu.
    ------------
    :param filt_wav: passband of the filter.
	:param filt_QE: quantum efficient of the filter.
 	:param spec_wav: rest-wavelengths (in microns).
    :param nuLnu: nuLnu.
    :param z: redshift.
    ------------
    :return flux_Obs: observed flux on Earth of the source located at redshift z with luminosity nuLnu (in Jansky).
    """

	norm = integrate.trapz(filt_QE, x=c/filt_wav/1e-6)

	if Fnu == False:
		Fnu_0 = nuLnuToFnu(spec_wav, nuLnu, z) #Flux received on Earth:
	else:
		Fnu_0 = nuLnu

	lambda_0 = spec_wav * (1. + z) # Wavelenght of emission
	Fnu_0filt = np.interp(filt_wav, lambda_0, Fnu_0) # Move the template to grab the redshifted flux
	flux_Obs = integrate.trapz(Fnu_0filt*filt_QE, x = c/filt_wav/1e-6)/norm # This is the flux received on Earth

	return flux_Obs

def contmodel(theta, x):
	
	# x = 10**np.arange(0., 3., 0.01)

	lambdab2, alpha1, alpha2 = theta
	Bnu = np.zeros(len(x))
	Bnu[x < 19.] = PLmodel_jit(x[x < 19.], 18., alpha1, alpha2, 20.)
	Bnu[x >= 19.] = PLmodel_jit(x[x >= 19.], lambdab2, alpha2, -3.5, 2.)

	# plt.plot(x, Bnu, 'k-')
	# plt.plot(x, PLmodel_jit(x, 15., alpha1, alpha2, 20.), 'r.')
	# plt.plot(x, PLmodel_jit(x, lambdab2, alpha2, -3.5, 2.), 'g.')
	# plt.xscale('log')
	# plt.yscale('log')
	# plt.show()

	# pdb.set_trace()


	return Bnu


@njit(fastmath = True)
def contmodel_jit(theta, x):
    
	# x_red = 10**np.arange(0., 3., 0.1)

	Bnu = np.zeros(len(x))
	Bnu[x < 19.] = PLmodel_jit(x[x < 19.], 18., theta[1], theta[2], 20.)
	Bnu[x >= 19.] = PLmodel_jit(x[x >= 19.], theta[0], theta[2], -3.5, 2.)
	
	# plt.plot(x, Bnu, 'r.')

	# x = 10**np.arange(0., 3., 0.1)

	# Bnu = np.zeros(len(x))
	# Bnu[x < 19.] = PLmodel_jit(x[x < 19.], 18., theta[1], theta[2], 20.)
	# Bnu[x >= 19.] = PLmodel_jit(x[x >= 19.], theta[0], theta[2], -3.5, 2.)
	
	# plt.plot(x, Bnu, 'b-')
	# plt.yscale('log')
	# plt.xscale('log')
	# plt.show()

	# pdb.set_trace()
	return Bnu


@njit(fastmath = True)
def PLmodel_jit(x, lambdab, alpha1, alpha2, s):

	"""
    This function calculates the second broken power-law of the AGN model.
    ------------
    :param x: x-values.
	:param lambdab1: position of the break for the first broken power law.
	:param lambdab2: position of the break for the second broken power law.
 	:param alpha1: slope of the first power-law.
 	:param alpha2: slope of the second power-law.
 	:param alpha2: slope of the third power-law.
    ------------
    :return Bnu: corresponding double-broken power law evaluated at x, and normalised at 10 microns.
    """

	aTerm = (x/19.)**alpha1
	apow = abs(alpha2 - alpha1)*s
	Bnu = (aTerm * (1. + (x/lambdab)**apow)**(np.sign(alpha2 - alpha1)/s))/((1. + (19./lambdab)**apow)**(np.sign(alpha2-alpha1)/s))

	return Bnu

import matplotlib.pyplot as plt
@njit(fastmath = True)
def Si10model_jit(x, dSi):

	"""
    This function calculates the second broken power-law of the AGN model.
    ------------
    :param x: x-values.
	:param lambdab1: position of the break for the first broken power law.
	:param lambdab2: position of the break for the second broken power law.
 	:param alpha1: slope of the first power-law.
 	:param alpha2: slope of the second power-law.
 	:param alpha2: slope of the third power-law.
    ------------
    :return Bnu: corresponding double-broken power law evaluated at x, and normalised at 10 microns.
    """
	# x_red = 10**np.arange(0.85, 1.2, 0.03)
	# x_red[0] = x.min()
	# x_red[-1] = x.max()

	alpha1 = 18.
	alpha2 = -5.
	lambdab = 9.7 + dSi
	s = 0.6

	Bnu = x**alpha1*(1. + (x/lambdab)**(abs(alpha2-alpha1)*s))**(np.sign(alpha2-alpha1)/s)

	# plt.plot(x, Bnu, 'r.')
	# plt.yscale('log')
	# plt.xscale('log')
	# plt.show()

	# pdb.set_trace()

	return Bnu/Bnu.max()


def Si10model(x, dSi):

	"""
    This function calculates the second broken power-law of the AGN model.
    ------------
    :param x: x-values.
	:param lambdab1: position of the break for the first broken power law.
	:param lambdab2: position of the break for the second broken power law.
 	:param alpha1: slope of the first power-law.
 	:param alpha2: slope of the second power-law.
 	:param alpha2: slope of the third power-law.
    ------------
    :return Bnu: corresponding double-broken power law evaluated at x, and normalised at 10 microns.
    """

	alpha1 = 18.
	alpha2 = -5.
	lambdab = 9.7 + dSi
	s = 0.6

	Bnu = x**alpha1*(1. + (x/lambdab)**(abs(alpha2-alpha1)*s))**(np.sign(alpha2-alpha1)/s)

	return Bnu/Bnu.max()


@njit(fastmath = True)
def Si18model_jit(x, dSi):

	"""
    This function calculates the second broken power-law of the AGN model.
    ------------
    :param x: x-values.
	:param lambdab1: position of the break for the first broken power law.
	:param lambdab2: position of the break for the second broken power law.
 	:param alpha1: slope of the first power-law.
 	:param alpha2: slope of the second power-law.
 	:param alpha2: slope of the third power-law.
    ------------
    :return Bnu: corresponding double-broken power law evaluated at x, and normalised at 10 microns.
    """

	# x_red = 10**np.arange(0.95, 1.4, 0.03)
	# x_red[0] = x.min()
	# x_red[-1] = x.max()

	alpha1 = 70.
	alpha2 = -3.5
	lambdab = 12. + dSi
	s = 0.1

	Bnu = x**alpha1*(1. + (x/lambdab)**(abs(alpha2-alpha1)*s))**(np.sign(alpha2-alpha1)/s)
	# plt.plot(x, Bnu, 'r.')

	# # Bnu = 10**np.interp(np.log10(x), np.log10(x_red), np.log10(Bnu))

	# # plt.plot(x, Bnu, 'b-')

	# plt.xscale('log')
	# plt.yscale('log')
	# plt.show()

	# pdb.set_trace()

	return  Bnu/Bnu.max()

def Si18model(x, dSi):

	"""
    This function calculates the second broken power-law of the AGN model.
    ------------
    :param x: x-values.
	:param lambdab1: position of the break for the first broken power law.
	:param lambdab2: position of the break for the second broken power law.
 	:param alpha1: slope of the first power-law.
 	:param alpha2: slope of the second power-law.
 	:param alpha2: slope of the third power-law.
    ------------
    :return Bnu: corresponding double-broken power law evaluated at x, and normalised at 10 microns.
    """
	
	alpha1 = 70.
	alpha2 = -3.5
	lambdab = 12. + dSi
	s = 0.1

	Bnu = x**alpha1*(1. + (x/lambdab)**(abs(alpha2-alpha1)*s))**(np.sign(alpha2-alpha1)/s)

	return Bnu/Bnu.max()

















# @njit(fastmath = True)
# def Simodel_jit(x, theta):

# 	"""
#     This function calculates the second broken power-law of the AGN model.
#     ------------
#     :param x: x-values.
# 	:param lambdab1: position of the break for the first broken power law.
# 	:param lambdab2: position of the break for the second broken power law.
#  	:param alpha1: slope of the first power-law.
#  	:param alpha2: slope of the second power-law.
#  	:param alpha2: slope of the third power-law.
#     ------------
#     :return Bnu: corresponding double-broken power law evaluated at x, and normalised at 10 microns.
#     """
# 	x_red = 10**np.arange(0., 3., 0.1)

# 	Bnu = x_red**theta[1]*(1. + (x_red/theta[0])**(abs(theta[2]-theta[1])*theta[3]))**(np.sign(theta[2]-theta[1])/theta[3])

# 	return np.interp(x, x_red, Bnu/Bnu.max())#Bnu/Bnu.max()

# def Simodel(x, lambdab, alpha1, alpha2, s):

# 	"""
#     This function calculates the second broken power-law of the AGN model.
#     ------------
#     :param x: x-values.
# 	:param lambdab1: position of the break for the first broken power law.
# 	:param lambdab2: position of the break for the second broken power law.
#  	:param alpha1: slope of the first power-law.
#  	:param alpha2: slope of the second power-law.
#  	:param alpha2: slope of the third power-law.
#     ------------
#     :return Bnu: corresponding double-broken power law evaluated at x, and normalised at 10 microns.
#     """

# 	Bnu = x**alpha1*(1. + (x/lambdab)**(abs(alpha2-alpha1)*s))**(np.sign(alpha2-alpha1)/s)

# 	return Bnu/Bnu.max()







# @njit
# def Gauss_jit(x, mu, sigma):
# 	"""
#     This function calculates a Gaussian normalised to its maximum.
#     ------------
#     :param x: x-values.
# 	:param mu: mean.
#  	:param sigma: standard deviation.
#     ------------
#     :return Bnu: Gaussian evaluated at x, normalised to its maximum.
#     """

# 	Bnu = np.exp(-(np.log10(x)-np.log10(mu))**2./2./sigma/sigma)

# 	return Bnu/np.max(Bnu)


# class my_model(tt.Op):

# 	itypes = [tt.dvector]
# 	otypes = [tt.dvector]

# 	def __init__(self, model, x):

# 		self.model = model
# 		self.x = x

# 	def perform(self, node, inputs, outputs):
# 		theta, = inputs

# 		outputs[0][0] = np.array(self.model(theta, self.x), dtype = 'float64')

	# def grad(self, inputs, g):
	# 	# the method that calculates the gradients - it actually returns the
	# 	# vector-Jacobian product - g[0] is a vector of parameter values
	# 	theta, = inputs  # our parameters
	# 	pdb.set_trace()
	# 	return [g[0] * self.modelgrad(theta)]



# class myModelGrad(tt.Op):

# 	"""
# 	This Op will be called with a vector of values and also return a vector of
# 	values - the gradients in each dimension.
# 	"""
# 	itypes = [tt.dvector]
# 	otypes = [tt.dvector]

# 	def __init__(self, model, x):
# 		"""
# 		Initialise with various things that the function requires. Below
# 		are the things that are needed in this particular example.

# 		Parameters
# 		----------
# 		loglike:
# 		    The log-likelihood (or whatever) function we've defined
# 		data:
# 		    The "observed" data that our log-likelihood function takes in
# 		x:
# 		    The dependent variable (aka 'x') that our model requires
# 		sigma:
# 		    The noise standard deviation that out function requires.
# 		"""

# 		# add inputs as class attributes
# 		self.x = x
# 		self.model = model

# 	def perform(self, node, inputs, outputs):
# 		theta, = inputs

# 		# define version of likelihood function to pass to derivative function
# 		def modelcont(values):
# 			return self.model(values, self.x)

# 		# calculate gradients
# 		pdb.set_trace()
# 		grads = jacobian(modelcont)(theta)

# 		outputs[0][0] = grads






# define your super-complicated model that uses loads of external codes
# @as_op(itypes=[tt.dvector, tt.dvector], otypes=[tt.dvector])
# @vectorize([float64(float64, float64)])

# @guvectorize([(float64[:], float64, float64[:])], '(n),()->(n)')

# 	for i in range(x.shape[0]):
# 		y[i] = x[i] + theta
	# np.interp(x, x_red, Bnu)

	# pdb.set_trace()
	# plt.plot(x, Bnu*obs/interp1d(x, Bnu*obs)(25.), 'ko')
	# plt.plot(x, PLmodel_jit(x, 20., alpha1, alpha2, 20.), 'r.')
	# plt.plot(x, PLmodel_jit(x, lambdab2, alpha2, -3.5, 2.), 'g.')
	# plt.xscale('log')
	# plt.yscale('log')
	# plt.show()

	# pdb.set_trace()



# @as_op(itypes=[tt.dvector, tt.lscalar, tt.lscalar, tt.lscalar, tt.lscalar], otypes=[tt.dvector])


	# (x/25.)**alpha1 * (1. + (x/lambdab)**apow)**(sign/s)/(1. + (25./lambdab)**apow)**(sign/s)


	# aTerm = (x/25.)**alpha1
	# apow = abs(alpha2 - alpha1)*s

	# # if tt.max(Bnu).eval() != tt.max(Bnu).eval():
	# # 	return 0.





# define your really-complicated likelihood function that uses loads of external codes
# def my_loglike(theta, x, data, sigma):

# 	model = contmodel(theta, x)
# 	return -0.5 * (((data - model)/sigma)**2).sum()


# define a theano Op for our likelihood function
class LogLike(tt.Op):

	"""
	Specify what type of object will be passed and returned to the Op when it is
	called. In our case we will be passing it a vector of values (the parameters
	that define our model) and returning a single "scalar" value (the
	log-likelihood)
	"""
	itypes = [tt.dvector] # expects a vector of parameter values when called
	otypes = [tt.dscalar] # outputs a single scalar value (the log likelihood)

	def __init__(self, loglike, data, x, sigma):
		"""
		Initialise the Op with various things that our log-likelihood function
		requires. Below are the things that are needed in this particular
		example.

		Parameters
		----------
		loglike:
			The log-likelihood (or whatever) function we've defined
		data:
			The "observed" data that our log-likelihood function takes in
		x:
			The dependent variable (aka 'x') that our model requires
		sigma:
			The noise standard deviation that our function requires.
		"""

		# add inputs as class attributes
		self.likelihood = loglike
		self.data = data
		self.x = x
		self.sigma = sigma

	def perform(self, node, inputs, outputs):
		theta, = inputs  # this will contain my variables

        # call the log-likelihood function
		logl = self.likelihood(theta, self.x, self.data, self.sigma)

		outputs[0][0] = np.array(logl) # output the log-likelihood




def gradients(vals, func, releps=1e-3, abseps=None, mineps=1e-9, reltol=1e-3,
              epsscale=0.5):
    """
    Calculate the partial derivatives of a function at a set of values. The
    derivatives are calculated using the central difference, using an iterative
    method to check that the values converge as step size decreases.

    Parameters
    ----------
    vals: array_like
        A set of values, that are passed to a function, at which to calculate
        the gradient of that function
    func:
        A function that takes in an array of values.
    releps: float, array_like, 1e-3
        The initial relative step size for calculating the derivative.
    abseps: float, array_like, None
        The initial absolute step size for calculating the derivative.
        This overrides `releps` if set.
        `releps` is set then that is used.
    mineps: float, 1e-9
        The minimum relative step size at which to stop iterations if no
        convergence is achieved.
    epsscale: float, 0.5
        The factor by which releps if scaled in each iteration.

    Returns
    -------
    grads: array_like
        An array of gradients for each non-fixed value.
    """

    grads = np.zeros(len(vals))

    # maximum number of times the gradient can change sign
    flipflopmax = 10.

    # set steps
    if abseps is None:
        if isinstance(releps, float):
            eps = np.abs(vals)*releps
            eps[eps == 0.] = releps  # if any values are zero set eps to releps
            teps = releps*np.ones(len(vals))
        elif isinstance(releps, (list, np.ndarray)):
            if len(releps) != len(vals):
                raise ValueError("Problem with input relative step sizes")
            eps = np.multiply(np.abs(vals), releps)
            eps[eps == 0.] = np.array(releps)[eps == 0.]
            teps = releps
        else:
            raise RuntimeError("Relative step sizes are not a recognised type!")
    else:
        if isinstance(abseps, float):
            eps = abseps*np.ones(len(vals))
        elif isinstance(abseps, (list, np.ndarray)):
            if len(abseps) != len(vals):
                raise ValueError("Problem with input absolute step sizes")
            eps = np.array(abseps)
        else:
            raise RuntimeError("Absolute step sizes are not a recognised type!")
        teps = eps

    # for each value in vals calculate the gradient
    count = 0
    for i in range(len(vals)):
        # initial parameter diffs
        leps = eps[i]
        cureps = teps[i]

        flipflop = 0

        # get central finite difference
        fvals = np.copy(vals)
        bvals = np.copy(vals)

        # central difference
        fvals[i] += 0.5*leps  # change forwards distance to half eps
        bvals[i] -= 0.5*leps  # change backwards distance to half eps
        cdiff = (func(fvals)-func(bvals))/leps

        while 1:
            fvals[i] -= 0.5*leps  # remove old step
            bvals[i] += 0.5*leps

            # change the difference by a factor of two
            cureps *= epsscale
            if cureps < mineps or flipflop > flipflopmax:
                # if no convergence set flat derivative (TODO: check if there is a better thing to do instead)
                warnings.warn("Derivative calculation did not converge: setting flat derivative.")
                grads[count] = 0.
                break
            leps *= epsscale

            # central difference
            fvals[i] += 0.5*leps  # change forwards distance to half eps
            bvals[i] -= 0.5*leps  # change backwards distance to half eps
            cdiffnew = (func(fvals)-func(bvals))/leps
            if cdiffnew == cdiff:
                grads[count] = cdiff
                break

            # check whether previous diff and current diff are the same within reltol
            rat = (cdiff/cdiffnew)
            if np.isfinite(rat) and rat > 0.:
                # gradient has not changed sign
                if np.abs(1.-rat) < reltol:
                    grads[count] = cdiffnew
                    break
                else:
                    cdiff = cdiffnew
                    continue
            else:
                cdiff = cdiffnew
                flipflop += 1
                continue

        count += 1


    return grads


# define a theano Op for our likelihood function
class LogLikeWithGrad(tt.Op):

    itypes = [tt.dvector] # expects a vector of parameter values when called
    otypes = [tt.dscalar] # outputs a single scalar value (the log likelihood)

    def __init__(self, loglike, data, x, sigma):
        """
        Initialise with various things that the function requires. Below
        are the things that are needed in this particular example.

        Parameters
        ----------
        loglike:
            The log-likelihood (or whatever) function we've defined
        data:
            The "observed" data that our log-likelihood function takes in
        x:
            The dependent variable (aka 'x') that our model requires
        sigma:
            The noise standard deviation that out function requires.
        """

        # add inputs as class attributes
        self.likelihood = loglike
        self.data = data
        self.x = x
        self.sigma = sigma

        # initialise the gradient Op (below)
        self.logpgrad = LogLikeGrad(self.likelihood, self.data, self.x, self.sigma)

    def perform(self, node, inputs, outputs):
        # the method that is used when calling the Op
        theta, = inputs  # this will contain my variables

        # call the log-likelihood function
        logl = self.likelihood(theta, self.x, self.data, self.sigma)

        outputs[0][0] = np.array(logl) # output the log-likelihood

    def grad(self, inputs, g):
        # the method that calculates the gradients - it actually returns the
        # vector-Jacobian product - g[0] is a vector of parameter values
        theta, = inputs  # our parameters
        return [g[0]*self.logpgrad(theta)]


class LogLikeGrad(tt.Op):

    """
    This Op will be called with a vector of values and also return a vector of
    values - the gradients in each dimension.
    """
    itypes = [tt.dvector]
    otypes = [tt.dvector]

    def __init__(self, loglike, data, x, sigma):
        """
        Initialise with various things that the function requires. Below
        are the things that are needed in this particular example.

        Parameters
        ----------
        loglike:
            The log-likelihood (or whatever) function we've defined
        data:
            The "observed" data that our log-likelihood function takes in
        x:
            The dependent variable (aka 'x') that our model requires
        sigma:
            The noise standard deviation that out function requires.
        """

        # add inputs as class attributes
        self.likelihood = loglike
        self.data = data
        self.x = x
        self.sigma = sigma

    def perform(self, node, inputs, outputs):
        theta, = inputs

        # define version of likelihood function to pass to derivative function
        def lnlike(values):
            return self.likelihood(values, self.x, self.data, self.sigma)

        # calculate gradients
        pdb.set_trace()
        grads = gradients(theta, lnlike)

        outputs[0][0] = grads


 #        # the method that is used when calling the Op



	# def gradients(self, ) 


# # @as_op(itypes=[tt.dvector, tt.dscalar, tt.dscalar, tt.dscalar, tt.dscalar, tt.dscalar], otypes=[tt.dvector])
# def contmodel(x, lambdab1, lambdab2, alpha1, alpha2, alpha3):

# 	Bnu = np.zeros(len(x), dtype=object)
# 	Bnu[x < 25.] = PLmodel_jit(x[x < 25.], lambdab1, alpha1, alpha2, 20.)
# 	Bnu[x >= 25.] = PLmodel_jit(x[x >= 25.], lambdab2, alpha2, -3.5, 2.)

# 	# Bnu = tt.zeros_like(x, dtype = 'float64')
# 	# Bnu = tt.set_subtensor(Bnu[x < 25.], PLmodel_jit(x[x < 25.], lambdab1, alpha1, alpha2, 20.))
# 	# Bnu = tt.set_subtensor(Bnu[x >= 25.], PLmodel_jit(x[x >= 25.], lambdab2, alpha2, -3.5, 2.))

# 	return Bnu#.eval()












# @njit

# Drude profile
def drude(x, gamma_r, lambda_r, normed = True):
	"""
    This function calculates a Drude profile.
    ------------
    :param x: x-values.
    :param gamma_r: central wavelengths.
    :param lambda_r: fractional FWHM.
    ------------
    :keyword normed: if set to True, normalise to the maximum value.
    ------------
    :return drudeVal: the Drude profile evaluated at x.
    """
	numerateur = gamma_r**2.
	denominateur = (x/lambda_r - lambda_r/x)**2. + gamma_r**2.
	drudeVal = numerateur/denominateur

	if normed == True:
		return drudeVal/np.max(drudeVal)
	else:
		return np.max(drudeVal), drudeVal

def obsC(wavRest, flux):
	"""
    This function calculates the total obscuration at 9.7micron.
    ------------
    :param wavRest: rest-wavelength.
    :param flux: observed fluxes.
    ------------
    :return _tau9p7: the total obscuration at 9.7micron.
    """

	loc1 = np.where((wavRest >= 6.7) & (wavRest<=6.9))[0]
	loc2 = np.where((wavRest >= 29.) & (wavRest<=31.))[0]
	if len(loc2) == 0.:
		loc2 = np.where((wavRest >= 14.7) & (wavRest<=15.4))[0]

	if (len(loc1) <1) & (len(loc2) < 1):
	 	raise Exception()
	k = 1

	# Get the flux in the anchored wavelengths
	wavAbs = np.concatenate((wavRest[loc1], wavRest[loc2]))
	fluxAbs = np.concatenate((flux[loc1], flux[loc2]))

	# Calculate the continuum flux (unabsorbed)
	o = np.where(np.diff(wavAbs) > 0.)[0]
	spl = UnivariateSpline(np.log10(wavAbs[o]), np.log10(fluxAbs[o]), k=k)
	contFlux9p7 = np.interp(9.7,wavRest, 10**spl(np.log10(wavRest)))

	# Get the observed flux (absorbed)
	obsFlux9p7 = np.interp(9.6,wavRest, flux)

	# Ratio of the observed to continuum
	S9p7 = -np.log(obsFlux9p7/contFlux9p7)


	# plt.plot(wavRest, flux, 'k.')
	# plt.plot(wavRest, 10**spl(np.log10(wavRest)), 'r--')
	# plt.plot(9.7, obsFlux9p7, 'bo')
	# plt.plot(9.7, contFlux9p7, 'bo')
	# plt.xscale('log')
	# plt.yscale('log')
	# plt.show()

	if (S9p7 <= 0.):
		return -99.
	else:
		return S9p7 #S9p7toTau9p7(S9p7)

def getExtCurve(ExtCurve):

	# Open the right extinction curve
	if ExtCurve == 'iragnsep':
		EC = pd.read_csv(path_iragnsep+'/ExtCurves/iragnsep_extCurve.csv')
	elif ExtCurve == 'PAHfit':
		EC = pd.read_csv(path_iragnsep+'/ExtCurves/PAHfit_extCurve_updated.csv')
	elif ExtCurve == 'Min07':
		EC = pd.read_csv(path_iragnsep+'/ExtCurves/Min+07.csv')
	elif ExtCurve == 'CT06':
		EC = pd.read_csv(path_iragnsep+'/ExtCurves/CT06_extCurve.csv')

	return EC['lambda_mic'].values, EC['tau'].values


def S9p7toTau9p7(S9p7, source):

	if source == 'gal':
		tauvec = 10**np.arange(-5., 5., 0.01)
		S9p7vec = (1. - np.exp(-tauvec))/tauvec
		f = interp1d(S9p7vec, tauvec)

		fluxRatio = np.exp(-S9p7)

		return np.round(np.array([f(fluxRatio)])[0],3)

	if source == 'AGN':

		return S9p7


	